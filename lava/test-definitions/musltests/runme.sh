#!/bin/sh

set +x
mkdir -p /tmp
export TEST_RUNNER_TAGS='purecap fvp'
mkdir -p boot
mount /dev/sda2 boot/
cd boot/morello-musl-tests/
ash test-runner.sh > /log.txt 2>&1
cd -
umount boot

KNOWN_FAIL_LIST="static.pty static.test_groupentry static.test_fgetpwent \
	static.ns_parse static.openpty dynamic.pty dynamic.test_groupentry \
	dynamic.test_fgetpwent dynamic.ns_parse dynamic.openpty"
echo "<LAVA_SIGNAL_STARTTC musltest>"
for test in $(cat /log.txt | grep "PASS" | cut -d" " -f 3| cut -f1 -d":"); do
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=$test RESULT=pass>"
done
for test in $(cat /log.txt | grep "SKIP" | cut -d" " -f 3| cut -f1 -d":"); do
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=$test RESULT=skip>"
done
for test in $(cat /log.txt | grep "FAIL" | cut -d" " -f 3| cut -f1 -d":"); do
  skip=false
  for fail in ${KNOWN_FAIL_LIST}; do
    if [ "$test" = "$fail" ]; then
      skip=true
    fi
  done
  if [ $skip = true ]; then
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=$test RESULT=skip>"
  else
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=$test RESULT=fail>"
  fi
done
echo "<LAVA_SIGNAL_ENDTC musltest>"
