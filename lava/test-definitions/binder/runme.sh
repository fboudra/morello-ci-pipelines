#!/bin/sh

set +x

TESTS="binderDriverInterfaceTest binderLibTest binderSafeInterfaceTest"
TESTS="${TESTS} binderTextOutputTest"
for BUILD_SUFFIX in 64 c64; do
  for test in ${TESTS}; do
    case "${test}" in
      binderSafeInterfaceTest)
        adb shell "ulimit -n 128 && /data/nativetest${BUILD_SUFFIX}/${test}/${test}" 2>&1 \
          | tee /tmp/results.log
        ;;
      *)
        adb shell "ulimit -n 32768 && /data/nativetest${BUILD_SUFFIX}/${test}/${test}" 2>&1 \
          | tee /tmp/results.log
        ;;
    esac

    grep "\[==========\]" /tmp/results.log
    grep "\[  PASSED  \]" /tmp/results.log
    grep "\[  SKIPPED \]" /tmp/results.log
    grep "\[  FAILED  \]" /tmp/results.log

    grep -q "\[  FAILED  \]" /tmp/results.log
    status=$?
    echo "<LAVA_SIGNAL_STARTTC ${test}-${BUILD_SUFFIX}>"
    if [ "$status" -eq 1 ]; then
      echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test}-${BUILD_SUFFIX} RESULT=pass>"
    else
      echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test}-${BUILD_SUFFIX} RESULT=fail>"
    fi
    echo "<LAVA_SIGNAL_ENDTC ${test}-${BUILD_SUFFIX}>"
    rm -f /tmp/results.log
  done

  test="binderThroughputTest"
  adb shell "ulimit -n 32768 && /data/nativetest${BUILD_SUFFIX}/${test}/${test} -i 1000 -p" 2>&1 \
    | tee /tmp/results.log
  iterations=$(grep "iterations" /tmp/results.log | awk -F ':' '{ gsub(/ /,""); print $2 }')
  echo "<LAVA_SIGNAL_STARTTC ${test}-${BUILD_SUFFIX}>"
  if [ ${iterations} -gt 100000 ]; then
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test}-${BUILD_SUFFIX} RESULT=pass>"
  else
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test}-${BUILD_SUFFIX} RESULT=fail>"
  fi
  echo "<LAVA_SIGNAL_ENDTC ${test}-${BUILD_SUFFIX}>"
  rm -f /tmp/results.log
done
