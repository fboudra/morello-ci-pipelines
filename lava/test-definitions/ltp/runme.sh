#!/bin/sh

set +x

parse_ltp_output() {
    grep -E "PASS|FAIL|CONF"  "$1" \
        | awk '{print $1" "$2}' \
        | sed 's/PASS/pass/; s/FAIL/fail/; s/CONF/skip/'  >> "/tmp/result.txt"
}

mkdir -p /tmp
mkdir -p /etc
mkdir -p /opt
mkdir -p /dev/shm
mount -t tmpfs shmfs -o size=1G /dev/shm
mount /dev/sda2 /opt

echo "y" | /opt/ltp/IDcheck.sh
chmod 755 /opt
chmod 755 /opt/ltp
chmod 755 /opt/ltp/testcases
chmod 755 /opt/ltp/testcases/bin

/opt/ltp/runltp -p -f /opt/ltp/runtest/morello_transitional,/opt/ltp/runtest/morello_transitional_extended -l ltp.log

status=$?
cat /opt/ltp/results/ltp.log
parse_ltp_output /opt/ltp/results/ltp.log

echo "<LAVA_SIGNAL_STARTTC ltp>"
while read -r line; do
if echo "${line}" | grep -iq -E ".* +(pass|fail|skip|unknown)$"; then
   test="$(echo "${line}" | awk '{print $1}')"
   result="$(echo "${line}" | awk '{print $2}')"
   echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=$test RESULT=$result>"
fi
done < /tmp/result.txt
echo "<LAVA_SIGNAL_ENDTC ltp>"
