#!/bin/sh

set +x

curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo libjpeg-turbo.tar.xz "${LIBJPEG_TURBO_URL}"
curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo system.tar.xz "${SYSTEM_URL}"
tar -xf libjpeg-turbo.tar.xz
tar -xf system.tar.xz

adb shell mkdir -p /data/local/tmp/libjpeg-turbo-images
adb push libjpeg-turbo/testimages/* /data/local/tmp/libjpeg-turbo-images
adb shell mkdir -p /data/local/tmp/system/bin

for BUILD_SUFFIX in 64 c64; do
  adb push system/bin/tj${BUILD_SUFFIX} /data/local/tmp/system/bin
  adb push system/bin/dj${BUILD_SUFFIX} /data/local/tmp/system/bin
  adb push system/bin/cj${BUILD_SUFFIX} /data/local/tmp/system/bin
  adb push system/bin/tjunit${BUILD_SUFFIX} /data/local/tmp/system/bin
  adb push system/bin/jpegtran${BUILD_SUFFIX} /data/local/tmp/system/bin
  adb push system/bin/md5cmp${BUILD_SUFFIX} /data/local/tmp/system/bin
done

for BUILD_SUFFIX in 64 c64; do
  echo """set -ex
    cd /data/local/tmp && rm -rf tjbench_tile && mkdir tjbench_tile && cd tjbench_tile && \
    cp /data/local/tmp/libjpeg-turbo-images/testorig.ppm testout_tile.ppm && \
    /data/local/tmp/system/bin/tj${BUILD_SUFFIX} testout_tile.ppm 95 -rgb -quiet -tile -benchtime 0.01 -warmup 0 && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 89d3ca21213d9d864b50b4e4e7de4ca6 testout_tile_GRAY_Q95_8x8.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 847fceab15c5b7b911cb986cf0f71de3 testout_tile_420_Q95_8x8.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} d83dacd9fc73b0a6f10c09acad64eb1e testout_tile_422_Q95_8x8.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 7964e41e67cfb8d0a587c0aa4798f9c3 testout_tile_444_Q95_8x8.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 89d3ca21213d9d864b50b4e4e7de4ca6 testout_tile_GRAY_Q95_16x16.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} ca45552a93687e078f7137cc4126a7b0 testout_tile_420_Q95_16x16.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 35077fb610d72dd743b1eb0cbcfe10fb testout_tile_422_Q95_16x16.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 7964e41e67cfb8d0a587c0aa4798f9c3 testout_tile_444_Q95_16x16.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 89d3ca21213d9d864b50b4e4e7de4ca6 testout_tile_GRAY_Q95_32x32.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} d8676f1d6b68df358353bba9844f4a00 testout_tile_420_Q95_32x32.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} e6902ed8a449ecc0f0d6f2bf945f65f7 testout_tile_422_Q95_32x32.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 7964e41e67cfb8d0a587c0aa4798f9c3 testout_tile_444_Q95_32x32.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 89d3ca21213d9d864b50b4e4e7de4ca6 testout_tile_GRAY_Q95_64x64.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 4e4c1a3d7ea4bace4f868bcbe83b7050 testout_tile_420_Q95_64x64.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 2b4502a8f316cedbde1da7bce3d2231e testout_tile_422_Q95_64x64.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 7964e41e67cfb8d0a587c0aa4798f9c3 testout_tile_444_Q95_64x64.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 89d3ca21213d9d864b50b4e4e7de4ca6 testout_tile_GRAY_Q95_128x128.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} f24c3429c52265832beab9df72a0ceae testout_tile_420_Q95_128x128.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} f0b5617d578f5e13c8eee215d64d4877 testout_tile_422_Q95_128x128.ppm && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} 7964e41e67cfb8d0a587c0aa4798f9c3 testout_tile_444_Q95_128x128.ppm

  cd /data/local/tmp && rm -rf cjpeg_djpeg_444_islow_prog_crop && mkdir cjpeg_djpeg_444_islow_prog_crop && \
    cp /data/local/tmp/libjpeg-turbo-images/testorig.ppm . && \
    /data/local/tmp/system/bin/cj${BUILD_SUFFIX} -dct int -prog -sample 1x1 -outfile testout_444_islow_prog.jpg testorig.ppm && \
    /data/local/tmp/system/bin/dj${BUILD_SUFFIX} -dct int -crop 98x98+13+13 -ppm -outfile testout_444_islow_prog_crop98x98,13,13.ppm testout_444_islow_prog.jpg && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} db87dc7ce26bcdc7a6b56239ce2b9d6c testout_444_islow_prog_crop98x98,13,13.ppm

  cd /data/local/tmp && rm -rf jpegtran_crop && mkdir jpegtran_crop && cd jpegtran_crop && \
    cp /data/local/tmp/libjpeg-turbo-images/testorig.jpg . && \
    /data/local/tmp/system/bin/jpegtran${BUILD_SUFFIX} -crop 120x90+20+50 -transpose -perfect -outfile testout_crop.jpg testorig.jpg && \
    /data/local/tmp/system/bin/md5cmp${BUILD_SUFFIX} b4197f377e621c4e9b1d20471432610d testout_crop.jpg
""" > target-script-tjbench${BUILD_SUFFIX}.sh

  adb push target-script-tjbench${BUILD_SUFFIX}.sh /data/local/tmp/
  adb shell "sh /data/local/tmp/target-script-tjbench${BUILD_SUFFIX}.sh" > /tmp/results-tjbench${BUILD_SUFFIX}.txt 2>&1

  result=$?
  cat /tmp/results-tjbench${BUILD_SUFFIX}.txt
  if [ "$result" = "0" ]; then
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=tjbench${BUILD_SUFFIX} RESULT=pass>"
  else
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=tjbench${BUILD_SUFFIX} RESULT=fail>"
  fi

  echo """
    cd /data/local/tmp && rm -rf tjunittest && mkdir tjunittest && cd tjunittest && \
      /data/local/tmp/system/bin/tjunit${BUILD_SUFFIX} -alloc -nobuf
  """ > target-script-tjunittest${BUILD_SUFFIX}.sh


  adb push target-script-tjunittest${BUILD_SUFFIX}.sh /data/local/tmp/
  adb shell "sh /data/local/tmp/target-script-tjunittest${BUILD_SUFFIX}.sh" > /tmp/results-tjunittest${BUILD_SUFFIX}.txt 2>&1 
  result=$?
  cat /tmp/results-tjunittest${BUILD_SUFFIX}.txt
  if [ "$result" = "0" ]; then
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=tjunittest${BUILD_SUFFIX} RESULT=pass>"
  else
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=tjunittest${BUILD_SUFFIX} RESULT=fail>"
  fi

done
