#!/bin/sh

set +x

if [ "$TEST_TYPE" = "static" ]; then
  test="bionic-unit-tests-static"
elif [ "$TEST_TYPE" = "dynamic" ]; then
  test="bionic-unit-tests"
else
  echo "bionic test can be either static or dynamic"
  exit 1
fi

for test_path in ${TEST_PATHS}; do
  if [ "${GTEST_FILTER}x" != "x" ]; then
	  gtest_filter="--gtest_filter=${GTEST_FILTER}"
  else
	  gtest_filter=""
  fi
  adb shell "/data/${test_path}/${test}/${test} ${gtest_filter}" 2>&1 \
    | tee /tmp/results.log
  grep "\[==========\]" /tmp/results.log
  grep "\[  PASSED  \]" /tmp/results.log
  grep "\[  SKIPPED \]" /tmp/results.log
  grep "\[  FAILED  \]" /tmp/results.log

  grep -q "\[  FAILED  \]" /tmp/results.log
  status=$?
  echo "<LAVA_SIGNAL_STARTTC ${test_path}-${test}>"
  if [ "$status" -eq 1 ]; then
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test_path}-${test} RESULT=pass>"
  else
    echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=${test_path}-${test} RESULT=fail>"
  fi
  echo "<LAVA_SIGNAL_ENDTC ${test_path}-${test}>"
  rm -f /tmp/results.log
done
