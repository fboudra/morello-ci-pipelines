#!/bin/sh

set +x

curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo png-testfiles.tar.xz "${PNG_URL}"
curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo system.tar.xz "${SYSTEM_URL}"
tar -xf png-testfiles.tar.xz
tar -xf system.tar.xz

adb shell mkdir -p /data/local/tmp/system/bin
adb shell mkdir -p /data/local/tmp/png-testfiles

for BUILD_SUFFIX in 64 c64; do
  adb push system/lib${BUILD_SUFFIX}/libpng.so \
    /data/nativetest${BUILD_SUFFIX}/pngtest/
  adb push system/lib${BUILD_SUFFIX}/libz.so \
    /data/nativetest${BUILD_SUFFIX}/pngtest/
done

adb push png-testfiles/*.png \
  /data/local/tmp/png-testfiles

echo 'set -ex
cd /data/local/tmp/png-testfiles

for BUILD_SUFFIX in 64 c64; do
  LD_LIBRARY_PATH=/data/nativetest${BUILD_SUFFIX}/pngtest \
    /data/nativetest${BUILD_SUFFIX}/pngtest/pngtest rgb-alpha-16-sRGB.png
done
' > target-script-png.sh
adb push target-script-png.sh /data/local/tmp/
adb shell "sh /data/local/tmp/target-script-png.sh" > /tmp/png-log.txt 2>&1
result=$?
cat /tmp/png-log.txt
if [ "$result" = "0" ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=png RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=png RESULT=fail>"
fi
