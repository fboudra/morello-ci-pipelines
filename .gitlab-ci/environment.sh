#!/bin/bash

set -e

case "$CI_PROJECT_PATH" in
  morello/manifest)
    export MANIFEST_BRANCH=${MANIFEST_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/llvm-project)
    export LLVM_PROJECT_BRANCH=${LLVM_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/kernel/morello-ack)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/build-scripts)
    export BUILD_SCRIPTS_BRANCH=${BUILD_SCRIPTS_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/device/arm/morello)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/vendor/arm/morello-examples)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/vendor/arm/tools)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/libshim)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/libarchcap)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/art)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/bionic)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/build/make)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/build/soong)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/android-clat)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/boringssl)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/bzip2)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/compiler-rt)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/conscrypt)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/dnsmasq)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/e2fsprogs)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/freetype)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/googletest)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/icu)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/jemalloc_new)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/kernel-headers)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/libcxx)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/libcxxabi)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/libjpeg-turbo)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/libpng)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/libunwind_llvm)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/llvm)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/lzma)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/pcre)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/pdfium)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/selinux)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/sqlite)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/tcpdump)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/toybox)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/zlib)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/frameworks/base)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/frameworks/native)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/hardware/libhardware)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/system/core)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/system/libhidl)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/system/testing/gtest_extras)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/device/generic/goldfish)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/curl)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/gwp_asan)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/jsoncpp)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/libunwind)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/protobuf)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/external/python/cpython2)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/frameworks/av)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/hardware/interfaces)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/hardware/ril)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/packages/modules/dnsresolver)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/system/connectivity/wificond)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/system/extras)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/system/gsid)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/system/security)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/toolchain/llvm_android)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/android/platform/system/tools/hidl)
    export ANDROID_PROJECT_BRANCH=${ANDROID_PROJECT_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/scp-firmware)
    export SCP_BRANCH=${SCP_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/trusted-firmware-a)
    export TF_A_BRANCH=${TF_A_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/edk2)
    export EDK2_BRANCH=${EDK2_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/edk2-platforms)
    export EDK2_PLATFORM_BRANCH=${EDK2_PLATFORM_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/model-scripts)
    export MODEL_SCRIPTS_BRANCH=${MODEL_SCRIPTS_BRANCH:-"${CI_COMMIT_REF_NAME}"}
  ;;
  morello/board-firmware)
    export BOARDF_BRANCH=${BOARDF_BRANCH:-"${CI_COMMIT_REF_NAME}"}
esac

case "$CI_PROJECT_PATH" in
  */morello-ci-pipelines)
    if [ "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}" == "staging" ] || [ -z "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}" ] && [ "${CI_COMMIT_REF_NAME}" != "main" ]
    then
        export CI_PIPELINES_BRANCH="${CI_PIPELINES_BRANCH:-staging}"
    else
        export CI_PIPELINES_BRANCH="${CI_PIPELINES_BRANCH:-main}"
    fi
  ;;
  *)
    export CI_PIPELINES_BRANCH="${CI_PIPELINES_BRANCH:-main}"
    printf "INFO: Get Morello CI pipelines (%s)\n" "${CI_PIPELINES_BRANCH}"
    curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo /tmp/ci-pipelines.tar.bz2 \
      https://git.morello-project.org/morello/morello-ci-pipelines/-/archive/${CI_PIPELINES_BRANCH}/morello-ci-pipelines-${CI_PIPELINES_BRANCH}.tar.bz2 ;\
    tar -xf /tmp/ci-pipelines.tar.bz2 \
      morello-ci-pipelines-${CI_PIPELINES_BRANCH}/.gitlab-ci \
      morello-ci-pipelines-${CI_PIPELINES_BRANCH}/lava \
      --strip-components=1
  ;;
esac
