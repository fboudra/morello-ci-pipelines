#!/usr/bin/env python3
#
# This script builds and runs libc-test test suite.

import argparse
import glob
import platform
from argparse import Namespace
from pathlib import Path
from typing import Any, List

import utils
from utils import (
    ExecutionInfo,
    ExpectedFailures,
    Log,
    Remarks,
    Result,
    TemporaryResources,
)


class Assets:
    '''Helper to access test assets'''

    def __init__(self, args: Namespace) -> None:
        self.args = args

    @property
    def c_compiler_path(self) -> Path:
        return self.compiler_bin_path / 'clang'

    @property
    def ar_path(self) -> Path:
        return self.compiler_bin_path / 'llvm-ar'

    @property
    def ranlib_path(self) -> Path:
        return self.compiler_bin_path / 'llvm-ranlib'

    @property
    def junit_xml_path(self) -> Path:
        return self.output_path / 'libc_tests_junit_report.xml'

    def __getattr__(self, name: str) -> Any:
        return self.args.__getattribute__(name)


def build_libc_tests(assets: Assets) -> Result:
    '''Builds libc-test test suite'''
    Log.pretty().write('Building libc-test at ').highlight(assets.libc_test_path).write(
        ' into '
    ).highlight(assets.output_path).flush()

    makefile_path_str = str(assets.makefile_path)

    extra_make_flags = []
    if assets.verbose:
        extra_make_flags.append('Q=')

    extra_c_flags = []
    if assets.enable_cheriseed:
        extra_c_flags.append('-fsanitize=cheriseed -mabi=purecap')

    if assets.verbose:
        extra_c_flags.append('-g')

    if extra_c_flags:
        extra_c_flags = f"EXTRA_C_FLAGS={' '.join(extra_c_flags)}"

    commands = [
        [
            'make',
            '-rR',
            '-f',
            makefile_path_str,
            *extra_make_flags,
            f'LIBC_TEST_OUTPUT_PATH={assets.output_path}',
            'clean',
        ],
        [
            'make',
            '-rR',
            '-f',
            makefile_path_str,
            '-j',
            str(assets.nproc),
            f'CC={assets.c_compiler_path}',
            f'AR={assets.ar_path}',
            f'RANLIB={assets.ranlib_path}',
            f'TARGET_TRIPLE={assets.target_triple}',
            f'LIBC_TEST_PATH={assets.libc_test_path}',
            f'LIBC_TEST_OUTPUT_PATH={assets.output_path}',
            f'MUSL_LIBC_SYSROOT_PATH={assets.musl_libc_sysroot_path}',
            *extra_make_flags,
            extra_c_flags,
            'dynamic' if assets.dynamic else 'static',
        ],
    ]
    return utils.execute_commands(commands, assets.verbose, timeout=assets.timeout)


def run_libc_tests(assets: Assets, remarks: Remarks) -> Result:
    '''Runs libc-test test suite'''
    Log.pretty().write('Running libc-test at ').highlight(assets.output_path).flush()

    kind = 'dynamic' if assets.dynamic else 'static'
    runtest_path = str(assets.output_path / f'common/runtest.{kind}.bin')

    exec_info_list: List[ExecutionInfo] = []
    for binary in glob.iglob(str(assets.output_path / f'**/*.{kind}.bin')):
        if binary == runtest_path:
            continue
        category, name = (
            str(Path(binary).relative_to(assets.output_path)).lower().split('/', 1)
        )
        # 'API' is reserved on Jenkins, for example.
        if category.endswith('.api'):
            category += 's'
        category += '.dynamic' if assets.dynamic else '.static'
        exec_info = ExecutionInfo(
            name=name,
            category=category,
            command=[runtest_path, "-t", str(assets.timeout), binary],
            timeout=assets.timeout,
        )
        exec_info_list.append(exec_info)

    expected_failures = ExpectedFailures.parse_from_file(
        assets.expected_failures_path, remarks
    )
    return utils.execute_parallel(
        assets.nproc,
        exec_info_list,
        error_threshold=assets.error_threshold,
        testsuite_name=assets.testsuite_name,
        junit_xml_path=assets.junit_xml_path,
        allow_unstable=assets.allow_unstable,
        expected_failures=expected_failures,
    )


def parse_arguments() -> Namespace:
    '''Parses program arguments'''
    parser = argparse.ArgumentParser(
        description='Builds and runs libc-test test suite',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        '--output-path',
        dest='output_path',
        action='store',
        type=Path,
        default=utils.env_root_path('scratch/libc-test/'),
        help='output path to store the results',
    )
    parser.add_argument(
        '--libc-test-path',
        dest='libc_test_path',
        action='store',
        type=Path,
        default=utils.env_root_path('libc-test'),
        help='source path of libc-test',
    )
    parser.add_argument(
        '--makefile-path',
        dest='makefile_path',
        action='store',
        type=Path,
        default=utils.env_root_path('utils/libc-test/Makefile'),
        help='path to the Makefile to build the tests',
    )
    parser.add_argument(
        '--musl-libc-sysroot-path',
        dest='musl_libc_sysroot_path',
        action='store',
        type=Path,
        default=utils.env_root_path(
            f'musl-libc/obj/install/{platform.machine()}-linux-musl'
        ),
        help='path to musl-libc sysroot',
    )
    parser.add_argument(
        '--compiler-bin-path',
        dest='compiler_bin_path',
        action='store',
        type=Path,
        default=utils.env_root_path('llvm-project/build/bin'),
        help='path to the compiler binaries',
    )
    parser.add_argument(
        '--target-triple',
        dest='target_triple',
        action='store',
        type=str,
        default=f'{platform.machine()}-linux-musl',
        help='target triple',
    )
    parser.add_argument(
        '--enable-cheriseed',
        dest='enable_cheriseed',
        action='store_true',
        help='enables CHERIseed',
    )
    parser.add_argument(
        '--dynamic', dest='dynamic', action='store_true', help='test dynamic binaries'
    )
    utils.add_usual_arguments(parser, 'libc.test', 60.0)
    return parser.parse_args()


def main() -> Result:
    '''The main of the program'''
    assets = Assets(parse_arguments())
    utils.create_empty_directory(assets.output_path)
    resources = [
        assets.output_path / 'api',
        assets.output_path / 'common',
        assets.output_path / 'math',
        assets.output_path / 'functional',
        assets.output_path / 'regression',
    ]
    with TemporaryResources(resources, cleanup=assets.no_cleanup):
        with Remarks() as remarks:
            res = utils.try_do(build_libc_tests, assets)
            if not res.is_success():
                return res
            return run_libc_tests(assets, remarks)


if __name__ == '__main__':
    utils.main_wrapper(main)
