#!/usr/bin/env python3
#
# This script builds and runs llvm-test-suite's SingleSource and MultiSource
# tests.

import argparse
import json
import platform
from argparse import Namespace
from pathlib import Path
from string import Template
from typing import Any, List

import utils
from utils import (
    ElapsedTime,
    ExecutionInfo,
    ExecutionSummary,
    ExpectedFailures,
    Log,
    Remarks,
    Result,
    TemporaryResources,
)


class Assets:
    '''Helper to access test assets'''

    def __init__(self, args: Namespace) -> None:
        self.args = args

    @property
    def c_compiler_path(self) -> Path:
        return self.compiler_bin_path / 'clang'

    @property
    def cxx_compiler_path(self) -> Path:
        return self.compiler_bin_path / 'clang++'

    @property
    def linker_path(self) -> Path:
        return self.compiler_bin_path / 'ld.lld'

    @property
    def cache_file_path(self) -> Path:
        return self.output_path / 'cache.cmake'

    @property
    def crash_diagnostics_path(self) -> Path:
        return self.output_path / 'crashes'

    @property
    def test_run_log_path(self) -> Path:
        return self.output_path / 'results.json'

    @property
    def dynamic(self) -> bool:
        # Convention is to have the "dynamic" tag in the name of the CMake
        # cache file to indicate that it is meant to be a dynamic build.
        return 'dynamic' in str(self.cache_template_path)

    @property
    def junit_xml_path(self) -> Path:
        return self.output_path / 'llvm_test_suite_junit_report.xml'

    @property
    def resources(self) -> List[Path]:
        return [
            self.output_path / 'CMakeFiles',
            self.output_path / 'MultiSource',
            self.output_path / 'SingleSource',
            self.output_path / 'tools',
            self.crash_diagnostics_path,
            TemporaryResources.File(self.cache_file_path),
            TemporaryResources.File(self.output_path / 'cmake_install.cmake'),
            TemporaryResources.File(self.output_path / 'CMakeCache.txt'),
            TemporaryResources.File(self.output_path / 'lit.site.cfg'),
            TemporaryResources.File(self.output_path / 'Makefile'),
        ]

    def __getattr__(self, name: str) -> Any:
        return self.args.__getattribute__(name)


def generate_cmake_cache(assets: Assets) -> Result:
    '''Generates a cmake cache file from a template'''
    Log.pretty().write('Templating cache file from ').highlight(
        assets.cache_template_path
    ).write(' into ').highlight(assets.cache_file_path).flush(end='')

    template_values = {
        'MUSL_LIBC_SYSROOT_PATH': str(assets.musl_libc_sysroot_path),
        'TARGET_TRIPLE': assets.target_triple,
        'CRASH_DIAGNOSTICS_PATH': assets.crash_diagnostics_path,
    }

    result = None
    with open(assets.cache_template_path, 'r') as r_template:
        template = Template(r_template.read())
        result = template.substitute(template_values)

    with open(assets.cache_file_path, 'w') as w_cache:
        w_cache.writelines(result)

    return Result.success()


def build_llvm_test_suite(assets: Assets) -> Result:
    '''Builds llvm-test-suite'''
    Log.pretty().write('Building llvm-test-suite at ').highlight(
        assets.llvm_test_suite_path
    ).write(' into ').highlight(assets.output_path).flush()

    cmake_command = [
        'cmake',
        '-B',
        str(assets.output_path),
        '-C',
        str(assets.cache_file_path),
        '-DCMAKE_VERBOSE_MAKEFILE=1' if assets.verbose else '',
        '-DTEST_SUITE_COLLECT_COMPILE_TIME=ON',
        '-DTEST_SUITE_COLLECT_CODE_SIZE=ON',
        '-DTEST_SUITE_COLLECT_STATS=OFF',
        f'-DCMAKE_C_COMPILER={assets.c_compiler_path}',
        f'-DCMAKE_CXX_COMPILER={assets.cxx_compiler_path}',
        f'-DCMAKE_LINKER={assets.linker_path}',
        str(assets.llvm_test_suite_path),
    ]

    cmake_command += [
        '-DTEST_SUITE_RUN_BENCHMARKS=OFF',
        '-DTEST_SUITE_SUBDIRS=SingleSource;MultiSource',
    ]

    make_command = [
        'make',
        '-C',
        str(assets.output_path),
        f'-j{assets.nproc}',
        '-k',  # Keep going
    ]

    if assets.verbose:
        make_command.append('V=1')

    commands = [cmake_command, make_command]

    return utils.execute_commands(
        commands,
        assets.verbose,
        # Don't build with timeout: make doesn't interact well with timeouts
        # because of '--keep-going'.
        timeout=None,
        cwd=assets.llvm_test_suite_path,
    )


def run_llvm_test_suite(assets: Assets) -> Result:
    '''Runs llvm-test-suite'''
    Log.pretty().write('Running llvm-test-suite at ').highlight(
        assets.llvm_test_suite_path
    ).write(' into ').highlight(assets.output_path).flush()

    commands = [
        [
            # llvm-lit has fixed shebang to python3.8, which might not exist.
            'python3',
            str(assets.llvm_lit_path),
            f'-j{assets.nproc}',
            '--verbose' if assets.verbose else '--quiet',
            '-o',
            str(assets.test_run_log_path),
            str(assets.output_path),
        ]
    ]
    return utils.execute_commands(
        commands, assets.verbose, timeout=assets.timeout, cwd=assets.output_path
    )


def collect_results(
    assets: Assets, expected_failures: ExpectedFailures, duration: ElapsedTime
) -> Result:
    '''Collects results in an uniform way'''
    if not assets.test_run_log_path.is_file():
        Log.fail('Test results file is missing.')
        return Result.fail()

    exec_summary = ExecutionSummary(name=assets.testsuite_name)
    exec_summary.error_threshold = assets.error_threshold

    with open(assets.test_run_log_path, 'r') as r_file:
        results = json.load(r_file)
        for test_result in results['tests']:
            category, name = (
                test_result.get('name', '<unknown name>')
                .replace('test-suite :: ', '')
                .split('/', 1)
            )
            category += '.dynamic' if assets.dynamic else '.static'
            exec_info = ExecutionInfo(
                name=name,
                category=category,
                command='',
                elapsed_time=test_result.get('elapsed', '0'),
                stderr=test_result.get('output', ''),
            )
            print(f'{exec_info.name}')
            if test_result.get('code', 'FAIL') == 'PASS':
                exec_info.result = Result.success()
            else:
                exec_info.result = Result.fail()

            exec_info = expected_failures.check(exec_info)
            exec_summary.append(exec_info)

    return exec_summary.summarize(
        junit_xml_path=assets.junit_xml_path,
        duration=duration.elapsed_time_s(),
        allow_unstable=assets.allow_unstable,
    )


def parse_arguments() -> Namespace:
    '''Parses program arguments'''
    parser = argparse.ArgumentParser(
        description="Builds and runs llvm-test-suite's SingleSource and MultiSource tests.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        '--output-path',
        dest='output_path',
        action='store',
        type=Path,
        default=utils.env_root_path('scratch/llvm-test-suite/'),
        help='output path to store the results',
    )
    parser.add_argument(
        '--llvm-test-suite-path',
        dest='llvm_test_suite_path',
        action='store',
        type=Path,
        default=utils.env_root_path('llvm-test-suite'),
        help='source path of llvm-test-suite',
    )
    parser.add_argument(
        '--cache-template-path',
        dest='cache_template_path',
        action='store',
        type=Path,
        default=utils.env_root_path(
            f'utils/llvm-test-suite/O3-cheriseed-static.template.cmake'
        ),
        help='path to a template cmake cache snippet',
    )
    parser.add_argument(
        '--compiler-bin-path',
        dest='compiler_bin_path',
        action='store',
        type=Path,
        default=utils.env_root_path('llvm-project/build/bin'),
        help='path to the compiler binaries',
    )
    parser.add_argument(
        '--llvm-lit-path',
        dest='llvm_lit_path',
        action='store',
        type=Path,
        default=utils.env_root_path('llvm-project/llvm/utils/lit/lit.py'),
        help='path to llvm-lit',
    )
    parser.add_argument(
        '--musl-libc-sysroot-path',
        dest='musl_libc_sysroot_path',
        action='store',
        type=Path,
        default=utils.env_root_path(
            f'musl-libc/obj/install/{platform.machine()}-linux-musl'
        ),
        help='path to musl-libc sysroot',
    )
    parser.add_argument(
        '--target-triple',
        dest='target_triple',
        action='store',
        type=str,
        default=f'{platform.machine()}-linux-musl',
        help='target triple',
    )
    parser.add_argument(
        '--build-only',
        dest='build_only',
        action='store_true',
        help='only build, do not run',
    )
    utils.add_usual_arguments(parser, 'llvm.test.suite', 1800.0)
    return parser.parse_args()


def main() -> Result:
    '''The main of the program'''
    assets = Assets(parse_arguments())
    utils.create_empty_directory(assets.output_path)
    with TemporaryResources(assets.resources, cleanup=assets.no_cleanup):
        with Remarks() as remarks:
            res = utils.try_do(generate_cmake_cache, assets)
            if not res.is_success():
                return res
            # Overall elapsed time
            duration = ElapsedTime()
            res = utils.try_do(build_llvm_test_suite, assets)
            if assets.build_only:
                return res
            # res is not important here
            utils.try_do(run_llvm_test_suite, assets)
            expected_failures = ExpectedFailures.parse_from_file(
                assets.expected_failures_path, remarks
            )
            return collect_results(assets, expected_failures, duration)


if __name__ == '__main__':
    utils.main_wrapper(main)
