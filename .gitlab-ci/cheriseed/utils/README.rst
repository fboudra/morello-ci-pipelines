Test Driver Usage
=================

This file describes how to use test drivers in various circumstances.
Most of the test drivers share an almost identical set of command line options.

CHERIseed
---------

This section describes typical usage of test drivers with CHERIseed.

musl_libc_tests.py
^^^^^^^^^^^^^^^^^^

Minimum configuration:

.. code-block::

  /utils/musl_libc_tests.py \
    --enable-cheriseed \
    --expected-failures=musl_libc_test_expected_failures.json

Please see ``--help`` for default paths. There may be many additional paths to
configure depending on the container:

- compiler bin path
- musl-libc source path
- musl-libc sysroot path
- output path

In addition, ``--no-cleanup`` and ``--verbose`` flags might come handy to keep
all the results, so that those can be archived.

The test result is single JUnit xml file, by default at
``{{output_path}}/musl_libc_tests_junit_report.xml``.

libc_tests.py
^^^^^^^^^^^^^

Minimum configuration:

.. code-block::

  /utils/libc_tests.py \
    --enable-cheriseed \
    --expected-failures=libc_test_expected_failures.json

Similar to musl-libc test driver, there are additional paths to configure and
flags to consider.

The test result is single JUnit xml file, by default at
``{{output_path}}/libc_tests_junit_report.xml``.

llvm_test_suite.py
^^^^^^^^^^^^^^^^^^

Minimum configuration:

.. code-block::

  /utils/llvm_test_suite.py \
    --expected-failures=llvm_test_suite_expected_failures.json

Similar to musl-libc test driver, there are additional paths to configure and
flags to consider.

The test result is single JUnit xml file, by default at
``{{output_path}}/llvm_test_suite_junit_report.xml``.
