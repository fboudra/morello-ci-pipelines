#!/bin/bash

project_details=$(echo ${1} | base64 --decode)
echo $project_details | jq -c '.[]' | while read project_detail;
do
	PROJECT_ID=$(echo $project_detail| jq -r ."project_id")
	IID=$(echo $project_detail| jq -r ."iid")
	gitlab -c $PYTHON_GITLAB_CFG project-merge-request-note create --project-id $PROJECT_ID --mr-iid $IID --body "$2"
done
