#!/bin/sh

set -ex

CFLAGS="-static -Wl,--whole-archive -lpthread -Wl,--no-whole-archive" \
CXXFLAGS="-static -Wl,--whole-archive -lpthread -Wl,--no-whole-archive" \
./configure --host=aarch64-linux-gnu
make -j$(nproc)

(cd gdb; time tar -cJf ${CI_PROJECT_DIR}/morello-gdb.tar.xz gdb)

# Pass variables to test job
echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
echo "LAVA_TEMPLATE_NAME=fvp-gdb.yaml" >> ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env
