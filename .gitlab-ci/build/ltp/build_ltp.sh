#!/bin/bash

# SPDX-License-Identifier: BSD-3-Clause

set -e

[ -z ${SUBLEVEL} ] && SUBLEVEL=1 bash -c "$0"
[ -z ${SUBLEVEL} ] && exit

# config should go first
source "$(pwd)/config"
source "$(pwd)/utils"

mkdir -p ${PLAYGROUND}
pushd ${PLAYGROUND}
mkdir -p ${OUTPUT_DIR}

#-------------------------------
# Setup llvm
#-------------------------------
eval setup_project ${LLVM_SETUP[*]}

LLVM=${LLVM_SETUP[0]}
pushd ${LLVM}

mkdir -p ${PLAYGROUND}/.tmp/aarch64-unknown-linux-musl_purecap
mkdir -p ${PLAYGROUND}/.tmp/aarch64-unknown-linux-gnu
cp -Rfv ./lib/clang/13.0.0/lib/aarch64-unknown-linux-musl_purecap/* ${PLAYGROUND}/.tmp/aarch64-unknown-linux-musl_purecap
# The compiler-rt might not be provided for aarch64-unknown-linux-gnu for
# releases < 1.5
# Detecting the actual release version gets cumbersome as not all releases
# publish corresponding version file
LLVM_RELEASE_VER=$(get_release_version ${LLVM_SETUP[2]})
LLVM_RELEASE_VER_MAJOR=${LLVM_RELEASE_VER%%.*}
LLVM_RELEASE_VER_MINOR=${LLVM_RELEASE_VER##*.}

if [ ${LLVM_RELEASE_VER_MAJOR} -le 1 ] && [ ${LLVM_RELEASE_VER_MINOR} -lt 5 ]; then
   declare -r SKIP_PLAIN_AARCH64=1
fi

if [ -z ${SKIP_PLAIN_AARCH64} ]; then
    cp -Rfv ./lib/clang/13.0.0/lib/aarch64-unknown-linux-gnu/* ${PLAYGROUND}/.tmp/aarch64-unknown-linux-gnu
fi

# Get the actual remote name
setup_branch $(get_remote) "morello/baremetal-release-${LLVM_RELEASE_VER}"

mv ${PLAYGROUND}/.tmp/aarch64-unknown-linux-musl_purecap ./lib/clang/13.0.0/lib/

if [ -z ${SKIP_PLAIN_AARCH64} ]; then
    mv ${PLAYGROUND}/.tmp/aarch64-unknown-linux-gnu ./lib/clang/13.0.0/lib/
fi
rm ${PLAYGROUND}/.tmp -fr

export PATH=$(pwd)/bin:$PATH
popd

#-------------------------------
# Setup musl
#-------------------------------
eval setup_project ${MUSL_SETUP[*]}

MUSL=${MUSL_SETUP[0]}

pushd ${MUSL}
mkdir -p ${OUTPUT_DIR}/${MUSL}
mkdir -p ${OUTPUT_DIR}/sysroot
make clean
CC=clang ./configure \
    --disable-shared \
    --enable-morello \
    --disable-libshim \
    --target=aarch64-linux-musl_purecap \
    --prefix=${OUTPUT_DIR}/${MUSL}

if [ "$?" != 0 ]; then
    exit 1
else
    make -j$(get_jobs_nr)
    make install
fi

popd

if [ -z ${SKIP_PLAIN_AARCH64} ]; then
    MUSL_AARCH64=${MUSL}-aarch64

    pushd ${MUSL}
    mkdir -p ${OUTPUT_DIR}/${MUSL_AARCH64}
    make clean
    CC=clang ./configure \
        --disable-shared \
        --disable-morello \
        --disable-libshim \
        --target=aarch64-linux-gnu \
        --prefix=${OUTPUT_DIR}/${MUSL_AARCH64}

    if [ "$?" != 0 ]; then
        exit 1
    else
        make -j$(get_jobs_nr)
        make install
    fi
    popd
fi
#-------------------------------
# Setup linux for the uapi headers
#-------------------------------
eval setup_project ${LINUX_SETUP[*]}

KHDR_DIR="${PLAYGROUND}/${LINUX_SETUP[0]}"

export CC=clang
export HOST_CFLAGS="-O2 -Wall"
export HOST_LDFLAGS="-Wall"
export CONFIGURE_OPT_EXTRA="--prefix=/ --host=aarch64-linux-gnu --disable-metadata --without-numa"


#-------------------------------
# Setup ltp
#-------------------------------
eval setup_project ${LTP_SETUP[*]}


LTP=${LTP_SETUP[0]}
LTP_BUILD=${OUTPUT_DIR}/${LTP}

pushd $LTP

CFLAGS_COMMON="-g -isystem $KHDR_DIR/usr/include"
LDFLAGS_COMMON="-fuse-ld=lld -static"

# Purecap build:

TRIPLE=aarch64-linux-musl_purecap

CFLAGS_PURECAP="$CFLAGS_COMMON  --target=${TRIPLE} -march=morello+c64 \
                --sysroot=${OUTPUT_DIR}/${MUSL} -Wall"
LDFLAGS_PURECAP="$LDFLAGS_COMMON --target=${TRIPLE} -rtlib=compiler-rt \
                --sysroot=${OUTPUT_DIR}/${MUSL} -L$LTP_BUILD/lib"

MAKE_OPTS="TST_NEWER_64_SYSCALL=no TST_COMPAT_16_SYSCALL=no"                \
           TARGETS="pan testcases/kernel/syscalls tools/apicmds" BUILD_DIR="$LTP_BUILD"   \
           CFLAGS="$CFLAGS_PURECAP" LDFLAGS="$LDFLAGS_PURECAP"              \
           ./build.sh -t cross -o out -ip "${OUTPUT_DIR}/opt/ltp"

pushd ${OUTPUT_DIR}/opt/
tar -cf ltp.tar ltp/
cp ltp.tar ${CI_PROJECT_DIR}
popd

if [ -z ${SKIP_PLAIN_AARCH64} ]; then

    # Plain aarch64 build:
    TRIPLE=aarch64-linux-gnu
    LTP_AARCH64_BUILD=${LTP_BUILD}-aarch64

    CFLAGS_AARCH64="$CFLAGS_COMMON --target=${TRIPLE} \
                    --sysroot=${OUTPUT_DIR}/${MUSL_AARCH64}  -Wall"
    LDFLAGS_AARCH64="$LDFLAGS_COMMON  --target=${TRIPLE} -rtlib=compiler-rt \
                    --sysroot=${OUTPUT_DIR}/${MUSL_AARCH64} -L$LTP_AARCH64_BUILD/lib"


    MAKE_OPTS="TST_NEWER_64_SYSCALL=no TST_COMPAT_16_SYSCALL=no"                \
               TARGETS="pan testcases/kernel/syscalls tools/apicmds"                          \
               BUILD_DIR="$LTP_AARCH64_BUILD"                                   \
               CFLAGS="$CFLAGS_AARCH64" LDFLAGS="$LDFLAGS_AARCH64"              \
               ./build.sh -t cross -o out -ip "${OUTPUT_DIR}/opt/ltp-aarch64"

    pushd ${OUTPUT_DIR}/opt/
    tar -cf ltp-aarch64.tar ltp-aarch64/
    cp ltp-aarch64.tar ${CI_PROJECT_DIR}
    popd
fi
popd


echo 'Done'
