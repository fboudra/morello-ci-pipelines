#!/bin/sh

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"

set -ex

rm -rf .repo/manifests
if [ -z "${CI_COMMIT_TAG+x}" ]; then
    MANIFEST_CHECKOUT_BRANCH=${MANIFEST_BRANCH}
else
    MANIFEST_CHECKOUT_BRANCH="refs/tags/${CI_COMMIT_TAG}"
fi
repo init --depth=1 --no-tags --no-clone-bundle \
  -u https://git.morello-project.org/morello/manifest.git \
  -b ${MANIFEST_CHECKOUT_BRANCH} -g bsp

repo selfupdate
repo version

# For convenience, add gnulib and grub to the bsp group
xmlstarlet edit --inplace \
  --update "//project[@name='gnulib']/@groups" \
  --value android,bsp,busybox \
  .repo/manifests/morello-base.xml
xmlstarlet edit --inplace \
  --update "//project[@name='grub']/@groups" \
  --value android,bsp,busybox \
  .repo/manifests/morello-base.xml

time repo sync -j8 --quiet --no-clone-bundle
repo manifest -r -o pinned-manifest.xml
cat pinned-manifest.xml

# Skip downloads
ln -sf ${HOME}/tools ${CI_PROJECT_DIR}/tools
# Get rid of checksum
sed -i "s|^    \[checksum_url\]=.*|    \[checksum_url\]=\"\"|" build-scripts/fetch-tools.sh
if ! [ -z ${PLATFORM+x} ]; then
    # Build GRUB
    time bash -x ./build-scripts/fetch-tools.sh -f none -p $PLATFORM
    time bash -x ./build-scripts/build-grub.sh -f none -p $PLATFORM
	OUT_TGT_DIR="soc"
    cp -a output/$OUT_TGT_DIR/intermediates/grub.efi ${CI_PROJECT_DIR}
else
    # Build GRUB
    time bash -x ./build-scripts/fetch-tools.sh -f none
    time bash -x ./build-scripts/build-grub.sh -f none
	OUT_TGT_DIR="fvp"
    cp -a output/$OUT_TGT_DIR/intermediates/grub.efi ${CI_PROJECT_DIR}
fi


# Create SHA256SUMS.txt file
sha256sum grub.efi > SHA256SUMS.txt
