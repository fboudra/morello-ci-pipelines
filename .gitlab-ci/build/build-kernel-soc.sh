#!/bin/sh
set -ex
wget -O toolchain.zip https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/main/download?job=build-toolchain
unzip -o toolchain.zip
tar -xf morello-clang.tar.xz -C /tmp/
export PATH=/tmp/clang-current/bin/:$PATH

#Download LTP artifacts
LTP_BRANCH="morello/master"
if [ "${CI_COMMIT_REF_NAME}" = "morello/next" ]; then
  LTP_BRANCH="morello/next";
fi
wget --no-check-certificate https://git.morello-project.org/morello/morello-linux-ltp/-/jobs/artifacts/${LTP_BRANCH}/raw/ltp.tar?job=build-ltp -O ${CI_PROJECT_DIR}/ltp.tar
wget --no-check-certificate https://git.morello-project.org/morello/morello-linux-ltp/-/jobs/artifacts/${LTP_BRANCH}/raw/ltp-aarch64.tar?job=build-ltp -O ${CI_PROJECT_DIR}/ltp-aarch64.tar

#Append selftests config fragment
cat tools/testing/selftests/lib/config | tee -a arch/arm64/configs/morello_transitional_pcuabi_defconfig >/dev/null

make ARCH=arm64 LLVM=1 LLVM_IAS=1 morello_transitional_pcuabi_defconfig
make ARCH=arm64 LLVM=1 LLVM_IAS=1 -j$(nproc)

#Download musl tests artifacts
mkdir -p musl_test; cd musl_test
wget -O build-musl-aarch64.zip https://git.morello-project.org/morello/musl-libc/-/jobs/artifacts/morello/master/download?job=build-musl-aarch64
unzip build-musl-aarch64.zip
cd ..

wget -O artifacts.zip https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/main/download?job=build-busybox-soc
unzip -o artifacts.zip
xz -T 0 -d busybox.img.xz

cp busybox.img busybox-ltp.img

#Re-create partition2 of 20MB size to fit musl tests
sudo dd if=/dev/zero bs=1M count=20 >> busybox.img
echo -e "Fix\n" | parted ---pretend-input-tty busybox.img print
size=$(sudo parted busybox.img print | tail -n 2| head -n 1 | awk '{print $3}'| cut -f1 -d".")
size=$(( size + 20 ))
MB=MB
size="$size$MB"
sudo parted -s busybox.img resizepart 2 $size
part=$(sudo kpartx -av busybox.img | tail -n 1 | cut -d" " -f 3)
sudo mkfs.ext4 -F /dev/mapper/$part
sleep 10
mkdir -p boot_volume
sudo mount -o loop /dev/mapper/$part boot_volume
sudo tar -xf musl_test/morello-musl-tests-purecap.tar.gz -C boot_volume
sudo make ARCH=arm64 modules_install INSTALL_MOD_PATH=boot_volume
sudo umount -f boot_volume
sync
sudo kpartx -dv busybox.img

part=$(sudo kpartx -av busybox.img | head -n 1 | cut -d" " -f 3)
sudo mount -o loop /dev/mapper/$part boot_volume
sudo cp arch/arm64/boot/Image boot_volume/Image
make ARCH=arm64 headers_install
make -C tools/testing/selftests TARGETS="arm64 lib" ARM64_SUBTARGETS=morello ARCH=arm64 CC=clang install
cd tools/testing/selftests/
tar -cf kselftests.tar kselftest_install/
cp kselftests.tar $CI_PROJECT_DIR
sudo cp -rf kselftest_install/ ../../../boot_volume/
cd -
sudo umount -f boot_volume
sync
sudo kpartx -dv busybox.img
xz -T 0 busybox.img

#Re-create partition2 of 1.5G size to fit ltp tests
sudo dd if=/dev/zero bs=1M count=1536 >> busybox-ltp.img
echo -e "Fix\n" | sudo parted ---pretend-input-tty busybox-ltp.img print
size=$(sudo parted busybox-ltp.img print | tail -n 2| head -n 1 | awk '{print $3}'| cut -f1 -d".")
size=$(( size + 1536 ))
MB=MB
size="$size$MB"
sudo parted -s busybox-ltp.img resizepart 2 $size
sudo kpartx -av busybox-ltp.img > part_details
part=$(cat part_details | tail -n 1 | cut -d" " -f 3)
sudo mkfs.ext4 -F /dev/mapper/$part
sleep 10
mkdir -p boot_volume
sudo mount -o loop /dev/mapper/$part boot_volume
sudo tar -xf ${CI_PROJECT_DIR}/ltp.tar -C boot_volume
sudo umount -f boot_volume
sync

part=$(cat part_details | head -n 1 | cut -d" " -f 3)
sudo mount -o loop /dev/mapper/$part boot_volume
sudo cp arch/arm64/boot/Image boot_volume/Image
sudo umount -f boot_volume
sync
sudo kpartx -dv busybox-ltp.img
cp busybox-ltp.img busybox-ltp-morello.img

mkdir -p boot_volume
part=$(cat part_details | tail -n 1 | cut -d" " -f 3)
sudo mount -o loop /dev/mapper/$part boot_volume
sudo rm -rf boot_volume/ltp
sudo tar -xf ${CI_PROJECT_DIR}/ltp-aarch64.tar -C boot_volume
sudo mv boot_volume/ltp-aarch64 boot_volume/ltp
sudo umount -f boot_volume
sync
sudo kpartx -dv busybox-ltp.img
mv busybox-ltp.img busybox-ltp-aarch64.img
xz -T 0 busybox-ltp-aarch64.img
cp busybox-ltp-morello.img busybox-ltp.img
xz -T 0 busybox-ltp.img

# Pass variables to test job
echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env
