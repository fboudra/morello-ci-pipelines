#!/bin/sh

source .gitlab-ci/build/setup-android-build-env.sh

# Build Android
LUNCH_TGT="morello_nano-eng"
if ! [ -z ${PLATFORM+x} ]; then
	time bash -x ./build-scripts/fetch-tools.sh -f android-nano -p $PLATFORM
	time bash -x ./build-scripts/build-linux.sh -f android-nano -p $PLATFORM all
	time bash -x ./build-scripts/build-android.sh -f android-nano -p $PLATFORM all
	OUT_TGT_DIR="soc"
else
	time bash -x ./build-scripts/fetch-tools.sh -f android-nano
	time bash -x ./build-scripts/build-linux.sh -f android-nano all
	time bash -x ./build-scripts/build-android.sh -f android-nano all
	OUT_TGT_DIR="fvp"
fi

# Build tests
cd android

if [ "${CI_PROJECT_NAME}" = "llvm-project" ]; then
  LLVM_BRANCH="${CI_COMMIT_REF_NAME}"
else
  LLVM_BRANCH="morello/master"
fi
curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo /tmp/lldb_tests.tar.bz2 \
    https://git.morello-project.org/morello/llvm-project/-/archive/${LLVM_BRANCH}/llvm-project-morello-dev.tar.bz2?path=lldb/morello-android-tests
mkdir test/lldb && tar -xf /tmp/lldb_tests.tar.bz2 -C test/lldb --strip-components=3
export ENVSETUP_NO_COMPLETION=adb:fastboot:asuite
source build/envsetup.sh
time lunch ${LUNCH_TGT}
mmma test/lldb
TESTS="bionic-unit-tests bionic-unit-tests-static"
TESTS="${TESTS} binderDriverInterfaceTest binderLibTest binderSafeInterfaceTest"
TESTS="${TESTS} binderTextOutputTest binderThroughputTest"
TESTS="${TESTS} liblog-unit-tests liblog-benchmarks logd-unit-tests"
TESTS="${TESTS} tjbench djpeg cjpeg tjunittest jpegtran md5"
TESTS="${TESTS} pcre2test"
TESTS="${TESTS} pdfium_test"
TESTS="${TESTS} pngtest"
TESTS="${TESTS} zlib_bench minigzip_target"
TESTS="${TESTS} boringssl_ssl_test boringssl_crypto_test"

for test in ${TESTS}; do
  m -j ${test}
done
for example in $(ls -dR vendor/arm/morello-examples/*/); do
  if [ -e ${example}/Android.bp ]; then
    example=$(echo ${example} | cut -d'/' -f4)
    m -j ${example}
  fi
done

m -j userdataimage-nodeps snod

mkdir lldb_tests/
cp -rf out/target/product/morello/data/ \
       out/target/product/morello/symbols/ \
       lldb_tests/
cp -rf test/lldb/ lldb_tests/
XZ_OPT="-T0 -2" tar -cJf lldb_tests.tar.xz lldb_tests/
cp lldb_tests.tar.xz ${CI_PROJECT_DIR}

mkdir -p libjpeg-turbo/testimages/
cp -rf external/libjpeg-turbo/testimages/* libjpeg-turbo/testimages/
XZ_OPT="-T0 -2" tar -cJf libjpeg-turbo.tar.xz libjpeg-turbo/
cp libjpeg-turbo.tar.xz ${CI_PROJECT_DIR}

mkdir -p pdfium-testfiles/
cp -rf external/pdfium/testing/resources/*.pdf pdfium-testfiles/
XZ_OPT="-T0 -2" tar -cJf pdfium-testfiles.tar.xz pdfium-testfiles/
cp pdfium-testfiles.tar.xz ${CI_PROJECT_DIR}

mkdir -p png-testfiles/
cp -rf external/libpng/contrib/testpngs/*.png png-testfiles/
XZ_OPT="-T0 -2" tar -cJf png-testfiles.tar.xz png-testfiles/
cp png-testfiles.tar.xz ${CI_PROJECT_DIR}

cd out/target/product/morello/
XZ_OPT="-T0 -2" tar -cJf system.tar.xz system/
cp system.tar.xz ${CI_PROJECT_DIR}/

cd ${CI_PROJECT_DIR}

# Generate Android images
if ! [ -z ${PLATFORM+x} ]; then
	time bash -x ./build-scripts/build-disk-image.sh -f android-nano -p $PLATFORM
  FIRMWARE_PKG=output/$OUT_TGT_DIR/firmware/*.zip
else
	time bash -x ./build-scripts/build-disk-image.sh -f android-nano
fi

# Prepare files to publish
cp -a \
  android/out/target/product/morello/userdata.img \
  output/$OUT_TGT_DIR/android-nano.img \
  output/$OUT_TGT_DIR/firmware/*.bin \
  ${FIRMWARE_PKG} \
  ${CI_PROJECT_DIR}
(cd android/out/target/product/morello; time XZ_OPT="-T0 -2" tar -cJf ${CI_PROJECT_DIR}/userdata.tar.xz data)

# Compress the image
xz android-nano.img
xz userdata.img
# Create SHA256SUMS.txt file
sha256sum *.bin *.img.xz *.xml pdfium-testfiles.tar.xz png-testfiles.tar.xz libjpeg-turbo.tar.xz lldb_tests.tar.xz system.tar.xz userdata.tar.xz > SHA256SUMS.txt

# Pass variables to test job
echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
echo "TC_URL=${TC_URL:-https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/main/raw/lldb_tests.tar.xz?job=build-android}" >> ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env
