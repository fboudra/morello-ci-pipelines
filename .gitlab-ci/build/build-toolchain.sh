#!/bin/sh

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"
CHECK_TESTS="${CHECK_TESTS:-"check-llvm check-clang check-lld"}"

if [ -z "${ANDROID_GIT_COOKIE}" ]; then
  printf "INFO: Skip http.cookiefile\n"
else
  printf "android.googlesource.com\tFALSE\t/\tTRUE\t2147483647\to\t${ANDROID_GIT_COOKIE}\n" > ~/.gitcookies
  chmod 0600 ~/.gitcookies
  git config --global http.cookiefile ~/.gitcookies
  printf "INFO: Set http.cookiefile\n"
fi

set -ex

if [ -z "${SKIP_UPDATE}" ]; then
  rm -rf .repo/manifests
  if [ -z "${CI_COMMIT_TAG+x}" ]; then
    MANIFEST_CHECKOUT_BRANCH=${MANIFEST_BRANCH}
  else
    MANIFEST_CHECKOUT_BRANCH="refs/tags/${CI_COMMIT_TAG}"
  fi
  repo init --no-clone-bundle \
    -u https://git.morello-project.org/morello/manifest.git \
    -b ${MANIFEST_CHECKOUT_BRANCH} -g toolchain-src

  repo selfupdate
  repo version

  # Set the llvm-project revision
  case "$CI_PROJECT_PATH" in
    morello/llvm-project)
      LLVM_PROJECT_BRANCH="${LLVM_PROJECT_BRANCH:-morello/master}"
      TAG=$(curl -s https://git.morello-project.org/api/v4/projects/${CI_PROJECT_ID}/repository/tags/${LLVM_PROJECT_BRANCH}|jq '.name // empty')
      if [ ! -z $TAG ]; then
          LLVM_PROJECT_BRANCH=refs/tags/${LLVM_PROJECT_BRANCH}
      fi
      xmlstarlet edit --inplace \
        --update "//project[@name='llvm-project']/@revision" \
        --value ${LLVM_PROJECT_BRANCH} \
        .repo/manifests/morello-toolchain.xml
    ;;
    morello/android/toolchain/llvm_android)
      ANDROID_PROJECT_BRANCH="${ANDROID_PROJECT_BRANCH:-morello/master}"
      TAG=$(curl -s https://git.morello-project.org/api/v4/projects/${CI_PROJECT_ID}/repository/tags/${ANDROID_PROJECT_BRANCH}|jq '.name // empty')
      if [ ! -z $TAG ]; then
          ANDROID_PROJECT_BRANCH=refs/tags/${ANDROID_PROJECT_BRANCH}
      fi
      xmlstarlet edit --inplace \
        --update "//project[@name='android/toolchain/llvm_android']/@revision" \
        --value ${ANDROID_PROJECT_BRANCH} \
        .repo/manifests/morello-toolchain.xml
    ;;
  esac

  if [ ! -z ${PROJECT_REFS+x} ]; then
    ./.gitlab-ci/utils/patch_manifest_repo.sh ${PROJECT_REFS} .repo/manifests
    ./.gitlab-ci/utils/patch_manifest.sh ${PROJECT_REFS} .repo/manifests/morello-toolchain.xml
  fi

  # Avoid to download +12G of prebuilt binaries
  sed -i '/darwin/d' .repo/manifests/morello-toolchain.xml
  sed -i '/mingw32/d' .repo/manifests/morello-toolchain.xml
  sed -i '/windows/d' .repo/manifests/morello-toolchain.xml
  if [ -z "${ANDROID_GIT_COOKIE}" ]; then
    printf "INFO: Skip http.cookiefile\n"
  else
    xmlstarlet edit --inplace  \
      --update "//remote[@name='aosp']/@fetch" \
      --value "https://android.googlesource.com/a/" \
      .repo/manifests/remotes.xml
  fi

  time repo sync -j8 --quiet --no-clone-bundle
  repo manifest -r -o pinned-manifest.xml
  cat pinned-manifest.xml
fi

IS_RELEASE_BUILD=0
if grep -q "release" <<< "${LLVM_PROJECT_BRANCH}" || grep -q "release" <<< "${MANIFEST_BRANCH}" ; then
    cd toolchain-src/toolchain/llvm-project
    git fetch morello-gitlab-morello --unshallow
    cd -
    IS_RELEASE_BUILD=1
fi

export PATH=${CI_PROJECT_DIR}/toolchain-src/prebuilts/build-tools/linux-x86/bin:${CI_PROJECT_DIR}/toolchain-src/prebuilts/cmake/linux-x86/bin:${PATH}
which cmake
cmake --version

which python
python --version
which python3
python3 --version

if [ ! -z "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}" ]; then
    LLVM_TARGET_BRANCH=${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}
else
    LLVM_TARGET_BRANCH=${LLVM_PROJECT_BRANCH:-morello/dev}
fi

set +ex
OVERRIDE_SUPPORT=$(cat ./toolchain-src/toolchain/llvm_android/do_build.py | grep -c "override-branch-name")
set -ex

if [ $OVERRIDE_SUPPORT -eq 1 ] && [ $IS_RELEASE_BUILD -eq 0 ]; then
  OVERRIDE_ARGS="--override-branch-name ${LLVM_TARGET_BRANCH}"
else
  OVERRIDE_ARGS=""
fi

./toolchain-src/toolchain/llvm_android/build.py --install-llvm-utils --no-build windows "$@" ${OVERRIDE_ARGS}
cp -a toolchain-src/out/install/linux-x86/clang-dev ${CI_PROJECT_DIR}/clang-current
cp -a toolchain-src/out/stage2-install/bin/FileCheck ${CI_PROJECT_DIR}/clang-current/bin
if [ -z "${SKIP_ARCHIVE}" ]; then
  # Compress the toolchain
  cd ${CI_PROJECT_DIR}
  time XZ_OPT="-T0 -2" tar -cJf ${CI_PROJECT_DIR}/morello-clang.tar.xz clang-current
  # Create SHA256SUMS.txt file
  sha256sum *.tar.xz *.xml > SHA256SUMS.txt
fi

cd ${CI_PROJECT_DIR}/toolchain-src/out/stage2
LD_LIBRARY_PATH=${CI_PROJECT_DIR}/toolchain-src/out/stage2/lib64:${CI_PROJECT_DIR}/toolchain-src/out/stage2-install/lib64:${CI_PROJECT_DIR}/toolchain-src/out/lib/libxml2-linux \
PYTHONHOME=${CI_PROJECT_DIR}/toolchain-src/prebuilts/python/linux-x86 \
  ninja ${CHECK_TESTS} -v

# Pass variables to test job
echo "TC_URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/${CI_JOB_ID}/artifacts/morello-clang.tar.xz" > ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env
