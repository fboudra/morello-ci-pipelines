#!/usr/bin/bash
#
# This script builds CHERIseed LLVM toolchain from scratch.
# Cmake should be on PATH.

# Build CHERIseed compiler using 'build-morello.sh'.
readonly LLVM_PROJECT_INSTALL_PATH="${WORK_INSTALL_PATH}/llvm-project"
export LLVM_LIT_ARGS='-s --no-progress-bar --workers=20 --xunit-xml-output llvm-test-results.xml'
export LDFLAGS="-Wl,--strip-all"
bash "${BUILD_MORELLO_SCRIPT_PATH}" \
    clang-cheriseed \
    "${LLVM_PROJECT_PATH}" \
    "${HOST_LLVM_PATH}" \
    "${LLVM_PROJECT_INSTALL_PATH}" \
    "${LLVM_PROJECT_BUILD_PATH}"

unset LLVM_LIT_ARGS
unset LD_FLAGS

# Version is only available after build.
readonly CLANG_VERSION="$(get_clang_version "${LLVM_PROJECT_BUILD_PATH}")"

# Run CHERIseed compiler tests.
bash "${BUILD_MORELLO_SCRIPT_PATH}" \
    clang-test-cheriseed \
    "${LLVM_PROJECT_PATH}" \
    "${LLVM_PROJECT_BUILD_PATH}"

# Build CHERIseed musl-libc.
readonly SYSROOT_BUNDLE_NAME="cheriseed-sysroot-$(uname -m)-linux"
readonly MUSL_LIBC_INSTALL_PATH="${WORK_INSTALL_PATH}/${SYSROOT_BUNDLE_NAME}"
bash "${BUILD_MORELLO_SCRIPT_PATH}" \
    musl-cheriseed \
    "${MUSL_LIBC_PATH}" \
    "${MUSL_LIBC_INSTALL_PATH}" \
    "${LLVM_PROJECT_INSTALL_PATH}/bin" \
    "${LIBSHIM_PATH}"

# Package the toolchain.
readonly LLVM_PROJECT_BUNDLE_NAME="clang+llvm-${CLANG_VERSION}-cheriseed-$(uname -m)-linux-gnu-$(get_linux_id)-$(get_linux_release)"
tar -C "$(dirname "${LLVM_PROJECT_INSTALL_PATH}")" \
    --transform "s|$(basename "${LLVM_PROJECT_INSTALL_PATH}")|${LLVM_PROJECT_BUNDLE_NAME}|" \
    -cf "${LLVM_PROJECT_BUNDLE_NAME}.tar.xz" \
    -I "xz ${XZ_FLAGS}" \
    "$(basename "${LLVM_PROJECT_INSTALL_PATH}")"
create_sha256sum "${LLVM_PROJECT_BUNDLE_NAME}.tar.xz"

# Package sysroot.
tar -C "$(dirname "${MUSL_LIBC_INSTALL_PATH}")" \
    -cf "${SYSROOT_BUNDLE_NAME}.tar.xz" \
    -I "xz ${XZ_FLAGS}" \
    "$(basename "${MUSL_LIBC_INSTALL_PATH}")"
create_sha256sum "${SYSROOT_BUNDLE_NAME}.tar.xz"
