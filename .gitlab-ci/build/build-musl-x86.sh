#!/bin/sh

source .gitlab-ci/build/setup-musl-build-env.sh

# Remove artifacts from previous stage which we don't want to publish
rm -rf $WORKSPACE/*.tar.*

# Build Musl for Aarch64
${CI_PROJECT_DIR}/.gitlab-ci/build/build-toolchain-x86.sh $HOST_LLVM_PATH $LLVM_PROJECT $MUSL_PROJECT $MORELLO_HOME $WORKSPACE
