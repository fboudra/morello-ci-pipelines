#!/bin/sh

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"
TC_URL=${TC_URL:-}

install_custom_toolchain()
{
  test -z "${TC_URL}" && return 0
  TC="${CI_PROJECT_DIR}/morello-clang.tar.xz"
  test -f ${TC} || curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo ${TC} ${TC_URL}
  rm -rf ${CI_PROJECT_DIR}/tools/clang ${CI_PROJECT_DIR}/tools/.clang.*
  TC_DIR="${CI_PROJECT_DIR}/tools/clang/bin"
  mkdir -p $(dirname ${TC_DIR})
  tar -xf ${TC} -C $(dirname ${TC_DIR}) --strip-components=1
  export PATH="${TC_DIR}:${PATH}"
  printf "INFO: Custom toolchain installed from \n%s\n" "${TC_URL}"
  which clang
  clang --version
}

if [ "${PIPELINE}" = "toolchain" ]; then
  FW_URL="https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/${CI_PIPELINES_BRANCH}/raw"
  if ! [ -z ${PLATFORM+x} ]; then
      FW_DIR="output/${PLATFORM}/firmware"
      JOB="build-firmware-${PLATFORM}"
  else
      FW_DIR="output/fvp/firmware"
      JOB="build-firmware"
  fi
  FW_FILES="mcp_fw.bin mcp_romfw.bin scp_fw.bin scp_romfw.bin tf-bl1.bin fip.bin"
  mkdir -p ${CI_PROJECT_DIR}/${FW_DIR}
  for fw in ${FW_FILES}; do
    curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo \
      ${CI_PROJECT_DIR}/${FW_DIR}/${fw} "${FW_URL}/${FW_DIR}/${fw}?job=${JOB}"
  done
  if ! [ -z ${PLATFORM+x} ]; then
      FW_DIR="output/${PLATFORM}/intermediates"
  else
      FW_DIR="output/fvp/intermediates"
  fi
  FW_FILES="grub.efi morello.dtb"
  mkdir -p ${CI_PROJECT_DIR}/${FW_DIR}
  for fw in ${FW_FILES}; do
    curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo \
      ${CI_PROJECT_DIR}/${FW_DIR}/${fw} "${FW_URL}/${FW_DIR}/${fw}?job=${JOB}"
  done
fi

set -ex

rm -rf .repo/manifests
if [ -z "${CI_COMMIT_TAG+x}" ]; then
    MANIFEST_CHECKOUT_BRANCH=${MANIFEST_BRANCH}
else
    MANIFEST_CHECKOUT_BRANCH="refs/tags/${CI_COMMIT_TAG}"
fi
repo init \
  -u https://git.morello-project.org/morello/manifest.git \
  -b ${MANIFEST_CHECKOUT_BRANCH} -g busybox

repo selfupdate
repo version

case "$CI_PROJECT_PATH" in
  morello/build-scripts)
    BUILD_SCRIPTS_BRANCH=${BUILD_SCRIPTS_BRANCH:-morello/mainline}
    PROJECT_NAME=$(python3 .gitlab-ci/utils/gitlab-to-manifest.py)
    xmlstarlet edit --inplace \
      --update "//project[@name=\"${PROJECT_NAME}\"]/@revision" \
      --value ${BUILD_SCRIPTS_BRANCH} \
      .repo/manifests/morello-base.xml
  ;;
esac

if [ ! -z ${PROJECT_REFS+x} ]; then
  ./.gitlab-ci/utils/patch_manifest_repo.sh ${PROJECT_REFS} .repo/manifests
  for manifest in $(ls .repo/manifests/); do
    ./.gitlab-ci/utils/patch_manifest.sh ${PROJECT_REFS} .repo/manifests/${manifest}
  done
fi

time repo sync -j8
repo manifest -r -o pinned-manifest.xml
cat pinned-manifest.xml

# Skip downloads
mkdir -p tools/arm_gcc/bin
touch tools/arm_gcc/bin/arm-none-eabi-gcc
# Get rid of checksum
sed -i "s|^    \[checksum_url\]=.*|    \[checksum_url\]=\"\"|" build-scripts/fetch-tools.sh
# Install custom toolchain
install_custom_toolchain

# Build Busybox image
if ! [ -z ${PLATFORM+x} ]; then
	if ! [ -z ${NO_CAP+x} ]; then
		sed -i "s|stack_morello_capabilities=1|stack_morello_capabilities=0|g" ./build-scripts/config/bsp
	fi
	if ! [ -z ${NO_CAP+x} ] && ! [ -z ${USE_GCC+x} ]; then
		sed -i "s|=llvm|=gnu|g" ./build-scripts/config/bsp
	fi
	time bash -x ./build-scripts/fetch-tools.sh -f busybox -p ${PLATFORM}
	time bash -x ./build-scripts/build-linux.sh -f busybox -p ${PLATFORM} all
	time bash -x ./build-scripts/build-busybox.sh -f busybox -p ${PLATFORM} all
	time bash -x ./build-scripts/build-disk-image.sh -f busybox -p ${PLATFORM}
else
	time bash -x ./build-scripts/fetch-tools.sh -f busybox
	time bash -x ./build-scripts/build-linux.sh -f busybox all
	time bash -x ./build-scripts/build-busybox.sh -f busybox all
	time bash -x ./build-scripts/build-disk-image.sh -f busybox
fi

# Alternatively, we can run
# time bash -x ./build-scripts/build-all.sh -f busybox

# Prepare files to publish
if ! [ -z ${PLATFORM+x} ]; then
    BZYBOX_IMG_PATH="output/soc/busybox.img"
    FW_BIN_PATH="output/soc/firmware/*.bin"
    FW_PKG_PATH="output/soc/firmware/*.zip"
else
    BZYBOX_IMG_PATH="output/fvp/busybox.img"
    FW_BIN_PATH="output/fvp/firmware/*.bin"
fi
cp -a \
  ${BZYBOX_IMG_PATH} \
  ${FW_BIN_PATH} \
  ${FW_PKG_PATH} \
  ${CI_PROJECT_DIR}
# Compress the image
xz busybox.img

# Create SHA256SUMS.txt file
sha256sum *.bin *.img.xz *.xml > SHA256SUMS.txt

# Pass variables to test job
echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env
