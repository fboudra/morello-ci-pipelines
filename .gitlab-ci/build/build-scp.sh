#!/bin/sh

#git submodule sync --recursive
#git submodule update --init --recursive

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"

set -ex

rm -rf .repo/manifests
if [ -z "${CI_COMMIT_TAG+x}" ]; then
    MANIFEST_CHECKOUT_BRANCH=${MANIFEST_BRANCH}
else
    MANIFEST_CHECKOUT_BRANCH="refs/tags/${CI_COMMIT_TAG}"
fi
repo init --depth=1 --no-tags --no-clone-bundle \
  -u https://git.morello-project.org/morello/manifest.git \
  -b ${MANIFEST_CHECKOUT_BRANCH} -g bsp

repo selfupdate
repo version

# Set the SCP firmware revision
case "$CI_PROJECT_PATH" in
  morello/scp-firmware)
    SCP_BRANCH="${SCP_BRANCH:-morello/master}"
    xmlstarlet edit --inplace \
      --update "//project[@name='scp-firmware']/@revision" \
      --value ${SCP_BRANCH} \
      .repo/manifests/morello-base.xml
  ;;
esac

time repo sync -j8 --quiet --no-clone-bundle
repo manifest -r -o pinned-manifest.xml
cat pinned-manifest.xml

# Skip downloads
ln -sf ${HOME}/tools ${CI_PROJECT_DIR}/tools
# Get rid of checksum
sed -i "s|^    \[checksum_url\]=.*|    \[checksum_url\]=\"\"|" build-scripts/fetch-tools.sh

# Build SCP firmware
if ! [ -z ${PLATFORM+x} ]; then
	time bash -x ./build-scripts/fetch-tools.sh -f none -p $PLATFORM
	time bash -x ./build-scripts/build-scp.sh -f none -p $PLATFORM all
	time bash -x ./build-scripts/build-uefi.sh -f none -p $PLATFORM all
	time bash -x ./build-scripts/build-arm-tf.sh -f none -p $PLATFORM all
	time bash -x ./build-scripts/build-firmware-image.sh -f none -p $PLATFORM all
	OUT_TGT_DIR="soc"
    # Package board firmware
	curl https://git.morello-project.org/morello/board-firmware/-/archive/morello/mainline/board-firmware-morello-mainline.zip -o board-firmware.zip
	unzip board-firmware.zip
	cp ${CI_PROJECT_DIR}/output/$OUT_TGT_DIR/firmware/* board-firmware-morello-mainline/SOFTWARE/
	cd board-firmware-morello-mainline/
	zip -r ${CI_PROJECT_DIR}/output/soc/firmware/board-firmware.zip .
else
	time bash -x ./build-scripts/fetch-tools.sh -f none
    time bash -x ./build-scripts/build-scp.sh -f none all
	time bash -x ./build-scripts/build-uefi.sh -f none all
	time bash -x ./build-scripts/build-arm-tf.sh -f none all
	time bash -x ./build-scripts/build-firmware-image.sh -f none all
	OUT_TGT_DIR="fvp"
fi
cd ${CI_PROJECT_DIR}
cp -a output/$OUT_TGT_DIR/intermediates/*cp-*.bin ${CI_PROJECT_DIR}
if [ "${OUT_TGT_DIR}" == "fvp" ]; then
  cp -a bsp/rom-binaries/*.bin output/$OUT_TGT_DIR/firmware/
  cp -a bsp/rom-binaries/bl1.bin output/$OUT_TGT_DIR/firmware/tf-bl1.bin
fi
# Create SHA256SUMS.txt file
sha256sum *.bin output/$OUT_TGT_DIR/firmware/*.bin > SHA256SUMS.txt

echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
echo "LAVA_TEMPLATE_NAME=fvp-ubuntu.yaml" >> ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env
