#!/bin/sh

sudo apt update -q=2
# Install additional packages:
#  wget: used by musl's 'build-morello.sh' script.
#  netbase: used by some tests in musl-libc (test_getservbyname, test_getservbyport).
sudo apt install -q=2 --yes --no-install-recommends wget netbase

LLVM_PROJECT_BRANCH=${LLVM_PROJECT_BRANCH:-"morello/dev"}
CHECK_TESTS="${CHECK_TESTS:-"check-llvm check-clang check-lld check-lldb"}"
MUSL_PROJECT_BRANCH=${MUSL_PROJECT_BRANCH:-"morello/master"}

if [ -z "${ANDROID_GIT_COOKIE}" ]; then
  printf "INFO: Skip http.cookiefile\n"
else
  printf "android.googlesource.com\tFALSE\t/\tTRUE\t2147483647\to\t${ANDROID_GIT_COOKIE}\n" > ~/.gitcookies
  chmod 0600 ~/.gitcookies
  git config --global http.cookiefile ~/.gitcookies
  printf "INFO: Set http.cookiefile\n"
fi

if [ "${CI_PROJECT_PATH}" == "morello/musl-libc" ]; then
  MUSL_PROJECT_BRANCH="${CI_COMMIT_REF_NAME}"
fi

if [ "${CI_PROJECT_PATH}" == "morello/llvm-project" ]; then
  LLVM_PROJECT_BRANCH="${CI_COMMIT_REF_NAME}"
fi

set -ex

  if [ ! -z ${PROJECT_REFS+x} ]; then
    project_details=$(echo ${PROJECT_REFS} | base64 --decode)
    mkfifo tempPipe
    echo $project_details | jq -c '.[]' > tempPipe &
    while IFS= read -r project_detail
    do
	PROJECT_PATH=$(echo $project_detail| jq -r ."project_path")
        PROJECT_BRANCH=$(echo $project_detail| jq -r ."branch")
        PROJECT_NAME=$(echo ${PROJECT_PATH} | cut -d'/' -f2-)
        if [[ "$PROJECT_NAME" == "musl-libc" ]]; then
            MUSL_PROJECT_BRANCH=${PROJECT_BRANCH}
        fi
        if [[ "$PROJECT_NAME" == "llvm-project" ]]; then
            LLVM_PROJECT_BRANCH=${PROJECT_BRANCH}
        fi
    done < tempPipe
  fi

mkdir llvm-project
pushd llvm-project
git init
git fetch --no-tags --force --progress --depth=1 -- https://git.morello-project.org/morello/llvm-project +refs/heads/${LLVM_PROJECT_BRANCH}:refs/remotes/origin/${LLVM_PROJECT_BRANCH}
git checkout origin/${LLVM_PROJECT_BRANCH}
popd

# delete artifacts from previous stage
rm -rf musl-libc
git clone https://git.morello-project.org/morello/musl-libc -b $MUSL_PROJECT_BRANCH

which cmake
cmake --version
which python
python --version
which python3
python3 --version

# path to sources of the Morello toolchain
export LLVM_PROJECT_PATH="$(pwd)/llvm-project"
# where host LLVM is installed
export HOST_LLVM_PATH=/usr/lib/llvm-13
export MUSL_PATH=${PWD}/musl-libc
export MORELLO_LLVM_PATH=${PWD}/llvm-aarch64-morello-linux/
export BUILD_PATH=${PWD}/build/
export BUILD_RT_PATH=${PWD}/build-rt/
export AARCH64_SYSROOT=${PWD}/aarch64-sysroot/
export MORELLO_SYSROOT=${PWD}/morello-sysroot/

# version of morelloie
export MORELLOIE_PREFIX=${HOME}/morelloie
export MORELLOIE=${MORELLOIE_PREFIX}/bin/morelloie

if [ ! -z ${MORELLOIE_DOWNLOAD_URL+x} ]; then
  # install morelloie
  rm -rf ${HOME}/morelloie-* ${MORELLOIE_PREFIX}
  wget -q ${MORELLOIE_DOWNLOAD_URL}/morelloie-${MORELLOIE_VERSION}.tgz.sh -O ${HOME}/morelloie-${MORELLOIE_VERSION}.tgz.sh
  bash ${HOME}/morelloie-${MORELLOIE_VERSION}.tgz.sh --i-agree-to-the-contained-eula --prefix=${MORELLOIE_PREFIX}
fi

# Get clang version.
#
# This functions searches for the following line:
#   #define CLANG_VERSION X.Y.Z
# in
#   'Version.inc' of clang build
# and outputs
#   'X.Y.Z'
# without a newline.
#
# Arguments:
#   1: Path to the llvm build folder.
get_clang_version() {
    sed -nr 's/.*?CLANG_VERSION (.*)/\1/p' "${1}"/tools/clang/include/clang/Basic/Version.inc \
        | tr -d '\n'
}

# Get Linux distribution ID.
#
# This functions searches for the following line:
#   DISTRIB_ID=X
# in
#   /etc/lsb-release
# and outputs
#   'X'
# without a newline in lowercase.
#
# Arguments: none
get_linux_id() {
    sed -nr 's/DISTRIB_ID=(.*)/\1/p' /etc/lsb-release \
        | tr -d '\n' \
        | tr '[:upper:]' '[:lower:]'
}

# Get Linux version.
#
# This functions searches for the following line:
#   DISTRIB_RELEASE=X.Y
# in
#   /etc/lsb-release
# and outputs
#   'X.Y'
# without a newline.
#
# Arguments: none
get_linux_release() {
    sed -nr 's/DISTRIB_RELEASE=(.*)/\1/p' /etc/lsb-release \
        | tr -d '\n'
}

# Helper function to clone a repository efficiently.
#
# Arguments:
#   1: Repository URL.
#   2: Branch to checkout.
#   3: Output path.
#   Any extra arguments are passed to 'git clone'
clone_repository() {
    local -r URL="${1}"; shift
    local -r BRANCH="${1}"; shift
    local -r OUTPUT_PATH="${1}"; shift
    git clone --no-tags --single-branch "${URL}" -b "${BRANCH}" "${OUTPUT_PATH}" $@
}

# Helper function to create a SHA256 of a file.
#
# Arguments:
#   1: Path to the file for which the SHA256 should be calculated.
create_sha256sum() {
    local -r FILENAME="${1}"
    sha256sum "${FILENAME}" > "${FILENAME}.sha256"
}

# Returns the revision of HEAD of a repository.
#
# Arguments:
#   1: Path to the repository.
get_revision() {
    local -r REPOSITORY_PATH="${1}"
    git -C "${REPOSITORY_PATH}" rev-parse HEAD
}
