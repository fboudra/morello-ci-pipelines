# This script builds Morello LLVM toolchain from scratch.
# This includes Clang, Musl libc, CRT objects and compiler-rt.
# Musl libc is built for purecap target
# Cmake should be on PATH.

set -e
set -x

# env vars for AArch64-hosted build
export LLVM_LIT_ARGS='-s --no-progress-bar --workers=20 --xunit-xml-output llvm-test-results.xml'
export LLVM_TARGETS='X86;AArch64'
export BUNDLE_NAME='llvm-morello-linux-x86'

# build clang
bash ${MUSL_PATH}/tools/build-morello.sh clang ${LLVM_PROJECT_PATH} ${HOST_LLVM_PATH} ${MORELLO_LLVM_PATH} ${BUILD_PATH}

# install Musl headers
export CC=${MORELLO_LLVM_PATH}/bin/clang
bash ${MUSL_PATH}/tools/build-morello.sh musl-headers ${MUSL_PATH} ${AARCH64_SYSROOT} aarch64-unknown-linux-gnu
bash ${MUSL_PATH}/tools/build-morello.sh musl-headers ${MUSL_PATH} ${MORELLO_SYSROOT} aarch64-unknown-linux-musl_purecap

# build CRT objects
export CC=${MORELLO_LLVM_PATH}/bin/clang
bash ${MUSL_PATH}/tools/build-morello.sh crt ${LLVM_PROJECT_PATH} ${AARCH64_SYSROOT} aarch64-unknown-linux-gnu
bash ${MUSL_PATH}/tools/build-morello.sh crt ${LLVM_PROJECT_PATH} ${MORELLO_SYSROOT} aarch64-unknown-linux-musl_purecap

# build compiler-rt
export CC=${MORELLO_LLVM_PATH}/bin/clang
bash ${MUSL_PATH}/tools/build-morello.sh compiler-rt ${LLVM_PROJECT_PATH} ${MORELLO_LLVM_PATH} ${BUILD_RT_PATH} ${AARCH64_SYSROOT} aarch64-unknown-linux-gnu
bash ${MUSL_PATH}/tools/build-morello.sh compiler-rt ${LLVM_PROJECT_PATH} ${MORELLO_LLVM_PATH} ${BUILD_RT_PATH} ${MORELLO_SYSROOT} aarch64-unknown-linux-musl_purecap

# build purecap Musl
rm -rf ${MORELLO_SYSROOT}
export CC=${MORELLO_LLVM_PATH}/bin/clang
bash ${MUSL_PATH}/tools/build-morello.sh musl ${MUSL_PATH} ${MORELLO_SYSROOT} -- aarch64-unknown-linux-musl_purecap

# compile (but don't run) tests
export CC=${MORELLO_LLVM_PATH}/bin/clang
bash ${MUSL_PATH}/tools/build-morello.sh musl-test ${MUSL_PATH} ${MORELLO_SYSROOT} aarch64-unknown-linux-musl_purecap -- YES

# package clang
rm -vf ${BUNDLE_NAME}*.tar.gz
bash ${MUSL_PATH}/tools/build-morello.sh package ${MORELLO_LLVM_PATH} ${BUNDLE_NAME}

echo "All done"
