#!/bin/sh

set -ex

LLVM_PROJECT_BRANCH="${LLVM_PROJECT_BRANCH:-morello/master}"

rm -rf .git/

sudo apt update
sudo apt install -y autoconf automake libtool pkg-config clang bison cmake ninja-build samba flex texinfo time libglib2.0-dev libpixman-1-dev libarchive-dev libarchive-tools libbz2-dev libattr1-dev libcap-ng-dev mercurial

git config --global --add safe.directory $PWD

git clone https://github.com/CTSRD-CHERI/cheribuild
mkdir -p cheribuild/src cheribuild/build cheribuild/output cheribuild/cheribsd-test-results-purecap-kernel \
    cheribuild/cheribsd-test-results-hybrid-kernel cheribuild/webkit-test-results
ssh-keygen -t rsa -N '' -f /home/ci-runner/.ssh/cheribuild;
cat << EOF > cheribuild/cheribuild.json
{
  "source-root": "${CI_PROJECT_DIR}/cheribuild/src",
  "build-root": "${CI_PROJECT_DIR}/cheribuild/build",
  "output-root": "${CI_PROJECT_DIR}/cheribuild/output",
  "make-jobs": 4
}
EOF
echo "y" | ./cheribuild/cheribuild.py disk-image-morello-purecap -d;

# Pass variables to test job
echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env
