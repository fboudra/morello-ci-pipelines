#!/bin/sh

BASE_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/${BUILD_JOB_ID}/artifacts"
LLDB_URL="$BASE_URL/lldb_tests.tar.xz"
ROOTFS="--rootfs $BASE_URL/android-nano.img.xz"
USERDATA="$BASE_URL/userdata.tar.xz"
PNG_URL="$BASE_URL/png-testfiles.tar.xz"
SYSTEM_URL="$BASE_URL/system.tar.xz"
LIBJPEG_TURBO_URL="$BASE_URL/libjpeg-turbo.tar.xz"
PDFIUM_URL="$BASE_URL/pdfium-testfiles.tar.xz"
PARAMETERS=""
TESTS=""
AP_ROMFW="tf-bl1.bin"

case $TEST_NAME in
    binder)
        DEVICE="fvp-morello-android"
        TESTS="--tests binder"
        ;;
    bionic)
        DEVICE="fvp-morello-android"
        if [ "$BIONIC_TEST_TYPE" = "dynamic" ]; then
            PARAMETERS="--parameters BIONIC_TEST_TYPE=dynamic --parameters GTEST_FILTER=$GTEST_FILTER"
        else
            PARAMETERS="--parameters GTEST_FILTER=$GTEST_FILTER"
        fi
        TESTS="--tests bionic"
        ;;
    boringssl)
        DEVICE="fvp-morello-android"
        PARAMETERS="--parameters SYSTEM_URL=$SYSTEM_URL"
        TESTS="--tests boringssl"
        ;;
    compartment)
        DEVICE="fvp-morello-android"
        PARAMETERS="--parameters USERDATA=$USERDATA"
        TESTS="--tests compartment"
        ;;
    device-tree)
        DEVICE="fvp-morello-android"
        TESTS="--tests device-tree"
        ;;
    dvfs)
        DEVICE="fvp-morello-android"
        TESTS="--tests dvfs"
        ;;
    libjpeg-turbo)
        DEVICE="fvp-morello-android"
        PARAMETERS="--parameters LIBJPEG_TURBO_URL=$LIBJPEG_TURBO_URL --parameters SYSTEM_URL=$SYSTEM_URL"
        TESTS="--tests libjpeg-turbo"
        ;;
    libpcre)
        DEVICE="fvp-morello-android"
        TESTS="--tests libpcre"
        ;;
    libpdfium)
        DEVICE="fvp-morello-android"
        PARAMETERS="--parameters PDFIUM_URL=$PDFIUM_URL --parameters SYSTEM_URL=$SYSTEM_URL"
        TESTS="--tests libpdfium"
        ;;
    libpng)
        DEVICE="fvp-morello-android"
        PARAMETERS="--parameters PNG_URL=$PNG_URL --parameters SYSTEM_URL=$SYSTEM_URL"
        TESTS="--tests libpng"
        ;;
   logd)
        DEVICE="fvp-morello-android"
        PARAMETERS="--parameters USERDATA=$USERDATA"
        TESTS="--tests logd"
        ;;
    morello-linux-docker-debian-dt)
	DEVICE="fvp-morello-grub"
        ROOTFS="--rootfs $BASE_URL/morello-fvp.img.xz"
        TESTS="--tests boot-debian-dt"
	;;
    morello-linux-docker-busybox-dt)
	DEVICE="fvp-morello-grub"
        ROOTFS="--rootfs $BASE_URL/morello-fvp.img.xz"
        TESTS="--tests boot-busybox-dt"
	;;
    multicore)
        DEVICE="fvp-morello-android"
        TESTS="--tests multicore"
        ;;
    busybox)
        DEVICE="fvp-morello-busybox"
        ROOTFS="--rootfs $BASE_URL/busybox.img.xz"
        ;;
    debian)
        DEVICE="fvp-morello-debian"
        ROOTFS="--rootfs $BASE_URL/debian.img.xz"
        TESTS="--tests debian-purecap"
	;;
    baremetal)
        DEVICE="fvp-morello-baremetal"
        AP_ROMFW="bl1.bin"
        ROOTFS=""
        ;;
    poky)
        DEVICE="fvp-morello-oe"
        ROOTFS="--rootfs $BASE_URL/core-image-minimal-morello-fvp.wic"
        TESTS="--tests fwts"
        ;;
    ubuntu)
        DEVICE="fvp-morello-ubuntu"
        ROOTFS=""
	BASE_URL="$BASE_URL/output/fvp/firmware/"
	;;
    zlib)
        DEVICE="fvp-morello-android"
        PARAMETERS="--parameters SYSTEM_URL=$SYSTEM_URL"
        TESTS="--tests zlib"
        ;;
    boottest)
        ROOTFS="--rootfs $BASE_URL/android-swr.img.xz"
        DEVICE="fvp-morello-android"
        TESTS="--tests boottest"
        ;;
    android-nano-boot)
        DEVICE="fvp-morello-android"
	;;
    virtiop9)
        DEVICE="fvp-morello-busybox"
        ROOTFS="--rootfs $BASE_URL/busybox.img.xz"
        TESTS="--tests virtiop9"
    ;;
    smc91x-android)
        DEVICE="fvp-morello-android"
        TESTS="--tests smc91x"
    ;;
    smc91x-busybox)
        DEVICE="fvp-morello-busybox"
        ROOTFS="--rootfs $BASE_URL/busybox.img.xz"
        TESTS="--tests smc91x"
    ;;
    virtio_net-android)
        DEVICE="fvp-morello-android"
        TESTS="--tests virtio_net"
    ;;
    virtio_net-busybox)
        DEVICE="fvp-morello-busybox"
        ROOTFS="--rootfs $BASE_URL/busybox.img.xz"
        TESTS="--tests virtio_net"
    ;;
    *)
        echo "Unknown TEST_NAME '$TEST_NAME'"
        exit 1
        ;;
esac

# Dump tuxsuite command and exit on failure
set -x
tuxsuite test \
    --device "$DEVICE" \
    --ap-romfw "$BASE_URL/$AP_ROMFW" \
    --mcp-fw "$BASE_URL/mcp_fw.bin" \
    --mcp-romfw "$BASE_URL/mcp_romfw.bin" \
    $ROOTFS \
    --scp-fw "$BASE_URL/scp_fw.bin" \
    --scp-romfw "$BASE_URL/scp_romfw.bin" \
    --fip "$BASE_URL/fip.bin" $TESTS $PARAMETERS \
    --json-out status.json

tuxsuite_exit_code=$?
python ./.gitlab-ci/utils/tuxsuite_to_junit.py > ${TEST_NAME}.xml
exit $tuxsuite_exit_code
