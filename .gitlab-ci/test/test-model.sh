#!/bin/sh

MODEL_SCRIPTS_BRANCH="${MODEL_SCRIPTS_BRANCH:-morello/mainline}"
set -x
curl -L -o firmware.zip https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/main/download?job=build-firmware
curl -L -o ${1}.img.xz https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/main/raw/${1}.img.xz?job=build-${1}
unxz ${1}.img.xz
unzip firmware.zip -d ..
rm -rf pinned-manifest.xml firmware.zip
mv ${1}.img ../output/fvp/
cd ..
./model-scripts/run_model.sh -m /opt/model/FVP_Morello/models/Linux64_GCC-6.4/FVP_Morello -f ${1} -j
booted=$?
exit $booted
