workflow:
  rules:
    - if: '$CI_MERGE_REQUEST_LABELS =~ /^.*ci skip.*/'
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_COMMIT_BRANCH =~ /^morello\/\S+/
      when: always

stages:
  - build-stage1
  - build-stage2
  - generate-test
  - test
  - publish-artifacts

build-firmware:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-firmware
  stage: build-stage1
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  artifacts:
    paths:
      - "output/fvp/firmware/*.bin"
      - "output/fvp/intermediates/*.bin"
      - "output/fvp/intermediates/*.dtb"
      - "output/fvp/intermediates/*.efi"
      - "pinned-manifest.xml"
    expire_in: 5 days

build-firmware-soc:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-firmware
  stage: build-stage1
  variables:
     PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME//-soc/}.sh
  artifacts:
    paths:
      - "output/soc/firmware/*.bin"
      - "output/soc/firmware/*.zip"
      - "output/soc/intermediates/*.bin"
      - "output/soc/intermediates/*.dtb"
      - "output/soc/intermediates/*.efi"
      - "pinned-manifest.xml"
    expire_in: 5 days

build-toolchain:
  rules:
    - if: '$CI_PROJECT_PATH == "morello/android/toolchain/llvm_android"'
      when: always
    - when: never
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-toolchain
  stage: build-stage1
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  artifacts:
    paths:
      - "morello-clang.tar.xz"
      - "pinned-manifest.xml"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env

build-android-swr:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-android
  stage: build-stage2
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  artifacts:
    paths:
      - "*.bin"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env
  needs:
    - job: build-firmware
    - job: build-toolchain
      optional: true

build-android-swr-soc:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-android
  stage: build-stage2
  variables:
     PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME//-soc/}.sh
  artifacts:
    paths:
      - "*.bin"
      - "*.zip"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env
  needs:
    - job: build-firmware-soc
    - job: build-toolchain
      optional: true

build-android-nano-soc:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-android
  stage: build-stage2
  variables:
     PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME//-soc/}.sh
  artifacts:
    paths:
      - "*.bin"
      - "*.zip"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "userdata.tar.xz"
      - "system.tar.xz"
      - "*-testfiles.tar.xz"
      - "lldb_tests.tar.xz"
      - "libjpeg-turbo.tar.xz"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env
  needs:
    - job: build-firmware-soc
    - job: build-toolchain
      optional: true

build-android-nano:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-android
  stage: build-stage2
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  artifacts:
    paths:
      - "*.bin"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "userdata.tar.xz"
      - "system.tar.xz"
      - "*-testfiles.tar.xz"
      - "lldb_tests.tar.xz"
      - "libjpeg-turbo.tar.xz"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env
  needs:
    - job: build-firmware
    - job: build-toolchain
      optional: true

generate-test:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: generate-test
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - python3 .gitlab-ci/test/generate-config.py
    - ytt -f .gitlab-ci/test/config.yml -f config-values.yml > generated-config.yml
    - cat generated-config.yml
  needs:
    - job: build-android-nano
  artifacts:
    paths:
      - generated-config.yml

test:
  stage: test
  trigger:
    include:
      - artifact: generated-config.yml
        job: generate-test
    strategy: depend

publish_artifacts:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: publish-artifacts
  script:
    - echo  "Archiving artifacts"
  artifacts:
    paths:
      - "morello-clang.tar.xz"
      - "*.bin"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "userdata.tar.xz"
      - "system.tar.xz"
      - "*-testfiles.tar.xz"
      - "lldb_tests.tar.xz"
      - "libjpeg-turbo.tar.xz"
      - "morello-clang-aarch64.tar.xz"
      - "musl-libc.tar.xz"
  needs:
    - job: build-android-swr
    - job: build-android-nano

build-busybox:
  rules:
    - if: '$CI_PROJECT_PATH == "morello/build-scripts"'
      when: always
    - when: never
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-android
  stage: build-stage2
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  artifacts:
    paths:
      - "*.bin"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env
  needs:
    - job: build-firmware

build-busybox-soc:
  rules:
    - if: '$CI_PROJECT_PATH == "morello/build-scripts"'
      when: always
    - when: never
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-android
  stage: build-stage2
  variables:
     PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME//-soc/}.sh
  artifacts:
    paths:
      - "*.bin"
      - "*.zip"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env
  needs:
    - job: build-firmware-soc

test-tuxsuite-busybox:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  rules:
    - if: '$CI_PROJECT_PATH == "morello/build-scripts"'
      when: always
    - when: never
  stage: test
  variables:
    TEST_NAME: busybox
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/tuxsuite.sh
  needs:
    - job: build-busybox
  tags:
    - arm64
  artifacts:
    when: always

test-tuxsuite-android-swr-boottest:
  retry:
    max: 1
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    TEST_NAME: boottest
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/tuxsuite.sh
  needs:
    - job: build-android-swr
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"
    reports:
        junit:
          - "*.xml"

test-android-nano-boottest-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-android-boot.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-binder-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-binder.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-compartment-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-compartment.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-logd-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-logd.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-device-tree-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-device-tree.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-dvfs-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-dvfs.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-lldb-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-lldb.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-bionic-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-bionic.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-multicore-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-multicore.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-swr-boottest-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-android-swr-boot.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-swr-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"


test-android-nano-libjpeg-turbo-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-libjpeg-turbo.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-libpcre-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-libpcre.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-libpdfium-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-libpdfium.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-libpng-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-libpng.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-zlib-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-zlib.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-boringssl-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-boringssl.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

build-cheriseed:
  rules:
    - if: '$CI_PROJECT_PATH == "morello/android/platform/external/libshim"'
      when: always
    - when: never
  variables:
    LLVM_PROJECT_BRANCH: cheriseed
    MUSL_PROJECT_BRANCH: cheriseed
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-toolchain:latest.amd64
  stage: build-stage1
  allow_failure: true
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  artifacts:
    paths:
      - "clang+llvm-*.tar.xz"
      - "clang+llvm-*.tar.xz.sha256"
      - "cheriseed-sysroot-*.tar.xz"
      - "cheriseed-sysroot-*.tar.xz.sha256"
      - "manifest.txt"
    expire_in: 15 days
    reports:
      junit:
        - work/build/llvm-project/projects/compiler-rt/test/cheriseed/llvm-test-results.xml
        - work/test/**/*_junit_report.xml
