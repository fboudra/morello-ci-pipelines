workflow:
  rules:
    - if: '$CI_MERGE_REQUEST_LABELS =~ /^.*ci skip.*/'
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_COMMIT_BRANCH =~ /^morello\/\S+/
      when: always
    - if: $CI_COMMIT_BRANCH == "cheriseed"
      when: always

stages:
  - build-stage1
  - build-stage2
  - build-stage3
  - generate-test
  - test
  - publish-artifacts

build-toolchain:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-toolchain
  stage: build-stage1
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  artifacts:
    paths:
      - "morello-clang.tar.xz"
      - "pinned-manifest.xml"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env

build-musl-aarch64:
  image: ${CI_REGISTRY}/morello/morello-ci-restricted-containers/morello-morelloie:latest.arm64
  variables:
    MORELLOIE_VERSION: 2.3-533
    MORELLOIE_DOWNLOAD_URL: https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/Morello/Development%20Tools/Morello%20Instruction%20Emulator
  stage: build-stage1
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  artifacts:
    when: always
    paths:
      - "*clang*.tar.gz"
      - "morello-musl-tests-*.tar.gz"
    expire_in: 15 days
    reports:
      junit:
          - ${CI_PROJECT_DIR}/musl-libc/test/*.xml
  tags:
    - arm64_fast

build-musl-x86:
  image: ${CI_REGISTRY}/morello/morello-ci-restricted-containers/morello-morelloie:latest.amd64
  stage: build-stage1
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  artifacts:
    paths:
      - "*clang*.tar.gz"
    expire_in: 15 days

build-and-test-cheribsd:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-cheribsd
  stage: build-stage1
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  artifacts:
    when: always
    reports:
        junit:
          - ${CI_PROJECT_DIR}/cheribuild/cheribsd-test-results-purecap-kernel/*/*.xml
          - ${CI_PROJECT_DIR}/cheribuild/cheribsd-test-results-hybrid-kernel/*/*.xml
          - ${CI_PROJECT_DIR}/cheribuild/webkit-test-results/*.xml

build-android-swr:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-android
  allow_failure: true
  variables:
    PIPELINE: toolchain
  stage: build-stage3
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  allow_failure: true
  artifacts:
    paths:
      - "*.bin"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env

build-android-nano-soc:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-android
  stage: build-stage3
  variables:
     PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME//-soc/}.sh
  artifacts:
    paths:
      - "*.bin"
      - "*.zip"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "userdata.tar.xz"
      - "system.tar.xz"
      - "*-testfiles.tar.xz"
      - "lldb_tests.tar.xz"
      - "libjpeg-turbo.tar.xz"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env
  needs:
    - job: build-toolchain
    - job: build-firmware-soc

build-android-swr-soc:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-android
  allow_failure: true
  variables:
    PIPELINE: toolchain
    PLATFORM: soc
  stage: build-stage3
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME//-soc/}.sh
  allow_failure: true
  artifacts:
    paths:
      - "*.bin"
      - "*.zip"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env

build-android-nano:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-android
  variables:
    PIPELINE: toolchain
  stage: build-stage3
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  allow_failure: true
  artifacts:
    paths:
      - "*.bin"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "userdata.tar.xz"
      - "system.tar.xz"
      - "*-testfiles.tar.xz"
      - "lldb_tests.tar.xz"
      - "libjpeg-turbo.tar.xz"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env

build-busybox:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-android
  variables:
    PIPELINE: toolchain
  stage: build-stage3
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  allow_failure: true
  artifacts:
    paths:
      - "*.bin"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env

build-busybox-soc:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-android
  variables:
    PIPELINE: toolchain
    PLATFORM: soc
  stage: build-stage3
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME//-soc/}.sh
  allow_failure: true
  artifacts:
    paths:
      - "*.bin"
      - "*.zip"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "SHA256SUMS.txt"
    expire_in: 5 days
    reports:
      dotenv: build.env

build-firmware:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-firmware
  stage: build-stage2
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  allow_failure: true
  artifacts:
    paths:
      - "output/fvp/firmware/*.bin"
      - "output/fvp/intermediates/*.bin"
      - "output/fvp/intermediates/*.dtb"
      - "output/fvp/intermediates/*.efi"
      - "pinned-manifest.xml"
    expire_in: 5 days

build-firmware-soc:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-firmware
  stage: build-stage2
  variables:
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME//-soc/}.sh
  allow_failure: true
  artifacts:
    paths:
      - "output/soc/firmware/*.bin"
      - "output/soc/firmware/*.zip"
      - "output/soc/intermediates/*.bin"
      - "output/soc/intermediates/*.dtb"
      - "output/soc/intermediates/*.efi"
      - "pinned-manifest.xml"
    expire_in: 5 days

generate-test:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: generate-test
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - python3 .gitlab-ci/test/generate-config.py
    - ytt -f .gitlab-ci/test/config.yml -f config-values.yml > generated-config.yml
    - cat generated-config.yml
  needs:
    - job: build-android-nano
  artifacts:
    paths:
      - generated-config.yml

test:
  stage: test
  needs:
    - job: build-android-nano
    - job: generate-test
  trigger:
    include:
      - artifact: generated-config.yml
        job: generate-test
    strategy: depend

test-tuxsuite-busybox:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    TEST_NAME: busybox
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/tuxsuite.sh
  allow_failure: true
  needs:
    - job: build-busybox
  tags:
    - arm64

publish_artifacts:
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: publish-artifacts
  script:
    - echo "Archiving artifacts"
  artifacts:
    expire_in: 30 days
    paths:
      - "morello-clang.tar.xz"
      - "*.bin"
      - "*.img.xz"
      - "pinned-manifest.xml"
      - "userdata.tar.xz"
      - "system.tar.xz"
      - "*-testfiles.tar.xz"
      - "lldb_tests.tar.xz"
      - "libjpeg-turbo.tar.xz"
      - "morello-clang-aarch64.tar.xz"
      - "musl-libc.tar.xz"
  needs:
    - job: build-toolchain
    - job: build-android-swr
    - job: build-android-nano
    - job: build-busybox

test-tuxsuite-android-swr-boottest:
  retry:
    max: 1
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    TEST_NAME: boottest
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/tuxsuite.sh
  needs:
    - job: build-android-swr
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-boottest-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-android-boot.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-binder-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-binder.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-compartment-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-compartment.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-logd-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-logd.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-device-tree-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-device-tree.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-dvfs-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-dvfs.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-lldb-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-lldb.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-bionic-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-bionic.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-nano-multicore-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-multicore.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-nano-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-busybox-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-busybox.yaml
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-busybox-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

test-android-swr-boottest-soc:
  retry:
    max: 2
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-lava
  stage: test
  variables:
    LAVA_TEMPLATE_NAME: soc-android-swr-boot.yaml
    PLATFORM: soc
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/${CI_JOB_STAGE}/test.sh
  needs:
    - job: build-android-swr-soc
  tags:
    - arm64
  artifacts:
    when: always
    reports:
        junit:
          - "*.xml"

build-cheriseed:
  rules:
    - if: '$CI_COMMIT_REF_NAME == "cheriseed"'
      when: always
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "cheriseed"'
      when: always
    - when: never
  variables:
    MUSL_PROJECT_BRANCH: cheriseed
  image: ${CI_REGISTRY}/morello/morello-ci-containers/morello-toolchain:latest.amd64
  stage: build-stage1
  allow_failure: true
  script:
    - source <(curl -s https://git.morello-project.org/morello/morello-ci-pipelines/-/raw/main/.gitlab-ci/environment.sh)
    - time ./.gitlab-ci/build/${CI_JOB_NAME}.sh
  artifacts:
    paths:
      - "clang+llvm-*.tar.xz"
      - "clang+llvm-*.tar.xz.sha256"
      - "cheriseed-sysroot-*.tar.xz"
      - "cheriseed-sysroot-*.tar.xz.sha256"
      - "manifest.txt"
    expire_in: 15 days
    reports:
      junit:
        - work/build/llvm-project/projects/compiler-rt/test/cheriseed/llvm-test-results.xml
        - work/test/**/*_junit_report.xml
